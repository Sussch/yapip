#include "aboutwidget.h"
#include "ui_aboutwidget.h"

AboutWidget::AboutWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AboutWidget)
{
    ui->setupUi(this);

    QPixmap icon;
    icon.load(":/icons/yapip");
    scene.addPixmap(icon);

    ui->gIcon->setScene(&scene);

    hide();
}

AboutWidget::~AboutWidget()
{
    delete ui;
}

void AboutWidget::on_btnOk_clicked()
{
    hide();

    emit accepted();
}

void AboutWidget::show()
{
    QWidget::show();
    ui->btnOk->setFocus();
}
