#ifndef NETINTERFACE_H
#define NETINTERFACE_H

#include <QObject>
#include <QList>
#include <QTimer>

#include <QtNetwork/QTcpServer>

#include "netserver.h"
#include "netclient.h"
#include "packet.h"

/**
 * @class NetInterface
 * Network interface
 */
class NetInterface : public QObject
{
    Q_OBJECT
public:
    explicit NetInterface(QObject *parent = 0);
    ~NetInterface();

    /**
     * Starts the interface
     */
    void start();
    /**
     * Stops the interface
     */
    void stop();

    /**
     * Adds a client for a contact
     */
    void addClient(Contact *contact);

    /**
     * Finds a client by its IP address
     * @param[in] ip_addr IP address
     * @return Pointer to the client or NULL when it was not found
     */
    NetClient *findClient(QString ip_addr);

    void removeClient(QString ip_addr);

    //! Server
    NetServer server;
    //! List of clients
    QList<NetClient *> clients;

    //! A global port configuration
    PortConfig portconfig;

signals:
    void clientAdded(NetClient *client);

    /**
     * Server received a packet from a subclient?
     * @param[in] subclient Pointer to the subclient that sent it to the server
     */
    void serverGotPacket(Packet packet, NetSubclient *subclient);
    /**
     * A client received a packet?
     * @param[in] packet Received packet
     * @param[in] pclient Pointer to the client that received the packet
     */
    void clientGotPacket(Packet packet, NetClient *pclient);

    /**
     * Handler for new connections
     * @param[in] subclient Pointer to the subclient that connected
     */
    void connectionAccepted(NetSubclient *subclient);

    /**
     * Signal of connection established
     * @param[in] client Pointer to the client that was connected
     */
    void gotConnected(NetClient *client);
    /**
     * Handler for lost connections
     * @param[in] client Pointer to the client that got disconnected
     */
    void connectionLost(NetClient *client);

public slots:
    /**
     * Handler to encrypt and send packet to a certain IP address
     * @param[in] targetIP Target IP address
     * @param[in] packet Packet to be encrypted and sent
     */
    void postPacket(QString targetIP, Packet packet);

private slots:
    /**
     * Handler to send data to a certain IP address
     * @param[in] targetIP Target IP address
     * @param[in] data Data to be sent
     */
    void sendMessage(QByteArray data, QString targetIP);

    /**
     * Handles a raw message to a server (from one of its subclients)
     * @param[in] data Raw message (needs to be decrypted)
     * @param[in] subclient Pointer to the subclient that sent it to the server
     */
    void _messageToServer(QByteArray data, NetSubclient *subclient);
    /**
     * Handles a raw message to a client
     * @param[in] data Raw message (needs to be decrypted)
     * @param[in] pclient Pointer to the client
     */
    void _messageToClient(QByteArray data, NetClient *pclient);
    /**
     * Handler to route received messages
     * @param[in] data Data to be routed
     * @param[in] user_data User data to distinguish the message
     */
    void routeMessage(QByteArray data, QString user_data);

    /**
     * Handler for new connections
     * @param[in] subclient Pointer to the subclient that connected
     */
    void connectionAcceptedSlot(NetSubclient *subclient);

    /**
     * Signal of connection established
     * @param[in] client Pointer to the client that was connected
     */
    void gotConnectedSlot(NetClient *client);
    /**
     * Handler for lost connections
     * @param[in] client Pointer to the client that got disconnected
     */
    void connectionLostSlot(NetClient *client);

private:
    bool started;
};

//! A global instance of the network interface
extern NetInterface g_network;

#endif // NETINTERFACE_H
