#ifndef ABOUTWIDGET_H
#define ABOUTWIDGET_H

#include <QWidget>
#include <QGraphicsScene>

namespace Ui {
class AboutWidget;
}

class AboutWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit AboutWidget(QWidget *parent = 0);
    ~AboutWidget();

signals:
    void accepted();

public slots:
    void show();
private slots:
    void on_btnOk_clicked();

private:
    Ui::AboutWidget *ui;

    QGraphicsScene scene;
};

#endif // ABOUTWIDGET_H
