#ifndef NETSERVER_H
#define NETSERVER_H

#include <QObject>
#include <QList>
#include <QTimer>
#include <QtNetwork/QTcpServer>
#include <QSignalMapper>

#include "portconfig.h"
#include "netsubclient.h"

/**
 * @class NetServer
 * Server
 */
class NetServer : public QObject
{
    Q_OBJECT
public:
    explicit NetServer(QObject *parent = 0);
    ~NetServer();
    
    /**
     * Starts the server
     */
    void start();

    void clear();

    /**
     * Finds a subclient by IP address
     * @return Pointer to the subclient or NULL when it was not found
     */
    NetSubclient *findSubclient(QString ip_address);

    //! Port configuration
    PortConfig portconfig;

signals:
    /**
     * Signal of message reception
     * @param[in] message Message that was received
     * @param[in] subclient Pointer to the subclient that sent it
     */
    void gotMessage(QByteArray message, NetSubclient *subclient);

    /**
     * Signal of a new connection (someone connected to the server)
     * @param[in] subclient Pointer to the accepted subclient
     */
    void connectionAccepted(NetSubclient *subclient);

public slots:
    /**
     * Sends data to a contact at a certain IP
     * @param[in] targetIP IP address of the target contact
     * @param[in] data Data to be sent
     */
    void sendData(QString targetIP, QByteArray data);

    /**
     * Handler for accepting a new connection
     */
    void acceptConnection();
    /**
     * Handler for received data
     * @param[in] sender Pointer to the NetSubclient that sent the data (needs type conversion)
     */
    void startRead(QObject *sender);

    void restart();

private slots:
    void pingTimerEvent();

    void gotError(QAbstractSocket::SocketError error);

    void kickSubclient(int index);

private:
    //! \todo Use QSslServer
    QTcpServer server;
    QList<NetSubclient *> subclients;
    QSignalMapper read_mapper;

    QTimer ping_timer;
};

#endif // NETSERVER_H
