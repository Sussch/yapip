# Add files and directories to ship with the application 
# by adapting the examples below.
# file1.source = myfile
# dir1.source = mydir
DEPLOYMENTFOLDERS = # file1 dir1

symbian:TARGET.UID3 = 0xE58CB51A

# Smart Installer package's UID
# This UID is from the protected range 
# and therefore the package will fail to install if self-signed
# By default qmake uses the unprotected range value if unprotected UID is defined for the application
# and 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# If your application uses the Qt Mobility libraries, uncomment
# the following lines and add the respective components to the 
# MOBILITY variable. 
# CONFIG += mobility
# MOBILITY +=

QT += network xml

SOURCES += main.cpp mainwindow.cpp \
    contact.cpp \
    contactgroup.cpp \
    contacts.cpp \
    netinterface.cpp \
    netclient.cpp \
    netserver.cpp \
    netsubclient.cpp \
    texteventfilter.cpp \
    portconfig.cpp \
    addcontactwidget.cpp \
    addgroupwidget.cpp \
    cryptopp561/rijndael.cpp \
    cryptopp561/cryptlib.cpp \
    cryptopp561/simple.cpp \
    cryptopp561/misc.cpp \
    cryptopp561/cpu.cpp \
    cryptopp561/filters.cpp \
    cryptopp561/rng.cpp \
    cryptopp561/queue.cpp \
    cryptopp561/fips140.cpp \
    cryptopp561/algparam.cpp \
    cryptopp561/mqueue.cpp \
    cryptopp561/rdtables.cpp \
    cryptopp561/zlib.cpp \
    cryptopp561/zinflate.cpp \
    cryptopp561/zdeflate.cpp \
    cryptopp561/adler32.cpp \
    aboutwidget.cpp \
    encryptsettingswidget.cpp \
    cryptinterface.cpp \
    portcfgwidget.cpp \
    cryptopp561/gcm.cpp \
    cryptopp561/osrng.cpp \
    cryptopp561/randpool.cpp \
    cryptopp561/authenc.cpp \
    cryptopp561/modes.cpp \
    cryptopp561/strciphr.cpp \
    cryptopp561/sha.cpp \
    cryptopp561/des.cpp \
    cryptopp561/hrtimer.cpp \
    cryptopp561/iterhash.cpp \
    cryptopp561/dessp.cpp \
    cryptopp561/pch.cpp \
    log.cpp \
    packet.cpp \
    filesharewidget.cpp \
    sharedfile.cpp \
    editcontactwidget.cpp \
    cryptopp561/trdlocal.cpp
HEADERS += mainwindow.h \
    contact.h \
    contactgroup.h \
    contacts.h \
    netinterface.h \
    netclient.h \
    netserver.h \
    netsubclient.h \
    texteventfilter.h \
    portconfig.h \
    addcontactwidget.h \
    addgroupwidget.h \
    cryptopp561/seckey.h \
    cryptopp561/secblock.h \
    cryptopp561/rijndael.h \
    cryptopp561/cryptlib.h \
    cryptopp561/aes.h \
    cryptopp561/simple.h \
    cryptopp561/misc.h \
    cryptopp561/stdcpp.h \
    cryptopp561/config.h \
    cryptopp561/cpu.h \
    cryptopp561/filters.h \
    cryptopp561/rng.h \
    cryptopp561/queue.h \
    cryptopp561/fips140.h \
    cryptopp561/algparam.h \
    cryptopp561/argnames.h \
    cryptopp561/mqueue.h \
    cryptopp561/zlib.h \
    cryptopp561/zinflate.h \
    cryptopp561/zdeflate.h \
    cryptopp561/adler32.h \
    aboutwidget.h \
    encryptsettingswidget.h \
    cryptinterface.h \
    portcfgwidget.h \
    cryptopp561/gcm.h \
    cryptopp561/osrng.h \
    cryptopp561/randpool.h \
    cryptopp561/authenc.h \
    cryptopp561/modes.h \
    cryptopp561/strciphr.h \
    cryptopp561/sha.h \
    cryptopp561/des.h \
    cryptopp561/hrtimer.h \
    cryptopp561/iterhash.h \
    cryptopp561/pch.h \
    cryptopp561/smartptr.h \
    log.h \
    packet.h \
    filesharewidget.h \
    sharedfile.h \
    editcontactwidget.h \
    cryptopp561/fltrimpl.h \
    cryptopp561/trdlocal.h \
    cryptopp561/words.h
FORMS += mainwindow.ui \
    addcontactwidget.ui \
    addgroupwidget.ui \
    aboutwidget.ui \
    encryptsettingswidget.ui \
    portcfgwidget.ui \
    filesharewidget.ui \
    editcontactwidget.ui

# Please do not modify the following two lines. Required for deployment.
include(deployment.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog

RESOURCES += \
    icons.qrc
