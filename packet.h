#ifndef PACKET_H
#define PACKET_H

#include <QObject>

/**
 * @class Packet
 * A YapIP packet
 */
class Packet
{
public:
    Packet();
    Packet(QByteArray array);

    enum PacketType
    {
        //! Used to check if a connection exists between contacts
        PT_PING = 0,
        //! Used to check the connection speed between contacts
        PT_BANDWIDTH_CHECK = 1,
        //! Simple text message
        PT_TEXT = 2,

        //! Used to notify of sharing a file
        PT_NOTE_SHARE_FILE = 0x10,
        //! Used to notify that a contact has accepted a shared file
        PT_NOTE_ACCEPT_FILE = 0x11,
        //! Used to notify that file packets are to follow
        PT_NOTE_START_FILE = 0x12,
        //! File contents
        PT_FILE = 0x15,
        //! Used to notify the target contact that the file has been sent
        PT_NOTE_END_FILE = 0x1F
    };

    /**
     * Updates packet timestamp
     */
    void setCurrentTimestamp();

    /**
     * Sets packet index (mostly used for file transfer)
     */
    void setIndex(quint32 index);

    /**
     * Converts byte array to packet
     */
    QByteArray fromByteArray(QByteArray array);
    /**
     * Converts packet to byte array
     */
    QByteArray toByteArray();

    /**
     * Sets packet data as text
     */
    void setText(QString text);
    /**
     * Gets packet data as text
     */
    QString getText();

    QString timestampToString();

    /**
     * Sets packet type.
     * \see PacketType
     */
    void setType(PacketType type);

    /**
     * Clear the packet
     */
    void clear();

    Packet &operator=(const Packet &packet);

    /**
     * Number of bytes occupied by the packet header
     */
    static int getOverhead();

    int getPacketLength();

    //! Timestamp in UTC
    quint64 timestamp;
    //! Packet index
    quint32 packet_index;
    //! Packet type
    quint16 packet_type;
    //! Length of the data field (in Bytes). Note that this is updated pretty much in toByteArray() only.
    quint16 data_length;
    //! Packet data
    QByteArray data;
};

#endif // PACKET_H
