#ifndef ADDCONTACTWIDGET_H
#define ADDCONTACTWIDGET_H

#include <QWidget>

namespace Ui {
class AddContactWidget;
}

class AddContactWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit AddContactWidget(QWidget *parent = 0);
    ~AddContactWidget();

    void setNickname(QString name);
    void setIPAddress(QString IPaddr);

signals:
    void accepted();
    void rejected();

public slots:
    void show();

private slots:
    void on_btnAdd_clicked();

    void on_btnCancel_clicked();

private:
    Ui::AddContactWidget *ui;
};

#endif // ADDCONTACTWIDGET_H
