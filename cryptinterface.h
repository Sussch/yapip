#ifndef CRYPTINTERFACE_H
#define CRYPTINTERFACE_H

#include <QObject>

class CryptInterface : public QObject
{
    Q_OBJECT
public:
    explicit CryptInterface(QObject *parent = 0);

    enum Algorithm
    {
        CT_PlainText,
        CT_AES
    };

    void setAlgorithm(Algorithm algo);
    void setNumKeys(int num);
    int getNumKeys() const;
    void addKey(QByteArray key);
    bool removeKey(int index);
    void genKeys();

    void clearKeys();

    QByteArray getKey(int id) const;

    /**
     * Formats the key to meet the requirements
     */
    QByteArray formatKey(QByteArray key) const;

    void setIV(QByteArray iv);
    
public slots:
    /**
     * Post data to be encrypted
     * @param[in] data Data to be encrypted
     * @param[in] user_data User data to identify the block of data
     */
    void decIn(QByteArray data, QString user_data);

    /**
     * Post data to be decrypted
     * @param[in] data Data to be decrypted
     * @param[in] user_data User data to identify the block of data
     */
    void encIn(QByteArray data, QString user_data);

signals:
    /**
     * A signal of the encrypted data being ready
     * @param[in] data Encrypted data
     * @param[in] user_data User data to identify the encrypted block of data
     */
    void encOut(QByteArray data, QString user_data);

    /**
     * A signal of the decrypted data being ready
     * @param[in] data Decrypted data
     * @param[in] user_data User data to identify the block of data
     */
    void decOut(QByteArray data, QString user_data);

private:
    Algorithm algorithm;

    int num_keys;
    QList<QByteArray> keys;
    QByteArray iv_data;
};

//! Encryption interface
extern CryptInterface g_crypt;

#endif // CRYPTINTERFACE_H
