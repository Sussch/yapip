#ifndef TEXTEVENTFILTER_H
#define TEXTEVENTFILTER_H

#include <QObject>
#include <QEvent>

/**
 * @class TextEventFilter
 * Text events filter that catches the Return key and emits the sendText signal
 */
class TextEventFilter : public QObject
{
    Q_OBJECT
public:
    explicit TextEventFilter(QObject *parent = 0);
    
    /**
     * The function where the filtering magic happens
     */
    virtual bool eventFilter(QObject *obj, QEvent *event);

signals:
    /**
     * Signal to send the text in the editbox with this filter installed
     */
    void sendText();
public slots:
    
};

#endif // TEXTEVENTFILTER_H
