#include "packet.h"
#include "log.h"

#include <QDateTime>
#include <QDataStream>

Packet::Packet()
{
    timestamp = 0;
    packet_type = PT_TEXT;
    packet_index = 0;
    data_length = 0;
}

Packet::Packet(QByteArray array)
{
    fromByteArray(array);
}

void Packet::setCurrentTimestamp()
{
    timestamp = QDateTime::currentMSecsSinceEpoch();
    g_log.report(QString(" Setting current timestamp (%1)").arg(timestamp));
}

void Packet::setIndex(quint32 index)
{
    packet_index = index;
}

QByteArray Packet::fromByteArray(QByteArray array)
{
    g_log.report("Packet::fromByteArray()");

    QDataStream dstream(array);
    // For some reason, QDataStream doesn't do this
#ifdef Q_LITTLE_ENDIAN
    dstream.setByteOrder(QDataStream::LittleEndian);
#else
    dstream.setByteOrder(QDataStream::BigEndian);
#endif

    g_log.report(QString(" Raw data: %1").arg(QString(array.toHex())));

    dstream >> timestamp >> packet_index >> packet_type >> data_length;

    array = array.right(array.length() - getOverhead());

    data = array.left(data_length);
    array = array.right(array.length() - data_length);

    g_log.report(QString(" Timestamp: %1").arg(timestamp));
    g_log.report(QString(" Index: %1").arg(packet_index));
    g_log.report(QString(" Type: %1").arg(packet_type));
    g_log.report(QString(" Data length: %1").arg(data_length));
    g_log.report(QString(" Remaining: %1").arg(array.length()));

    return array;
}

QByteArray Packet::toByteArray()
{
    QByteArray array;

    data_length = data.length();

    array.append(QByteArray::fromRawData((const char *) &timestamp, sizeof(timestamp)));
    array.append(QByteArray::fromRawData((const char *) &packet_index, sizeof(packet_index)));
    array.append(QByteArray::fromRawData((const char *) &packet_type, sizeof(packet_type)));
    array.append(QByteArray::fromRawData((const char *) &data_length, sizeof(data_length)));
    array.append(data);

    return array;
}

Packet &Packet::operator=(const Packet &packet)
{
    timestamp = packet.timestamp;
    packet_index = packet.packet_index;
    packet_type = packet.packet_type;
    data_length = packet.data_length;
    data = packet.data;
    return *this;
}

void Packet::setText(QString text)
{
    data = text.toAscii();
}

QString Packet::getText()
{
    return QString(data);
}

void Packet::setType(PacketType type)
{
    packet_type = type;
}

void Packet::clear()
{
    timestamp = 0;
    packet_index = 0;
    packet_type = 0;
    data_length = 0;
    data.clear();
}

int Packet::getOverhead()
{
    int ovrhd;
    Packet p;

    ovrhd  = (int) sizeof(p.timestamp);
    ovrhd += (int) sizeof(p.packet_index);
    ovrhd += (int) sizeof(p.packet_type);
    ovrhd += (int) sizeof(p.data_length);

    return ovrhd;
}

QString Packet::timestampToString()
{
    return QString::number((quint64) timestamp);
}

int Packet::getPacketLength()
{
    return getOverhead() + data.length();
}
