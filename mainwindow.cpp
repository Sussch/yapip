#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtCore/QCoreApplication>
#include <QMessageBox>
#include <QFileDialog>

#include <iostream>

#include "log.h"
#include "contacts.h"
#include "netinterface.h"
#include "cryptinterface.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    stackwidget_id = 0;

    // Custom initialization
    initStackup();
    initContactsTree();
    initNetwork();
    initChat();
}

MainWindow::~MainWindow()
{
    delete ui;
}

//=============================================
// Mobile stuff
//=============================================
void MainWindow::setOrientation(ScreenOrientation orientation)
{
#if defined(Q_OS_SYMBIAN)
    // If the version of Qt on the device is < 4.7.2, that attribute won't work
    if (orientation != ScreenOrientationAuto) {
        const QStringList v = QString::fromAscii(qVersion()).split(QLatin1Char('.'));
        if (v.count() == 3 && (v.at(0).toInt() << 16 | v.at(1).toInt() << 8 | v.at(2).toInt()) < 0x040702) {
            qWarning("Screen orientation locking only supported with Qt 4.7.2 and above");
            return;
        }
    }
#endif // Q_OS_SYMBIAN

    Qt::WidgetAttribute attribute;
    switch (orientation) {
#if QT_VERSION < 0x040702
    // Qt < 4.7.2 does not yet have the Qt::WA_*Orientation attributes
    case ScreenOrientationLockPortrait:
        attribute = static_cast<Qt::WidgetAttribute>(128);
        break;
    case ScreenOrientationLockLandscape:
        attribute = static_cast<Qt::WidgetAttribute>(129);
        break;
    default:
    case ScreenOrientationAuto:
        attribute = static_cast<Qt::WidgetAttribute>(130);
        break;
#else // QT_VERSION < 0x040702
    case ScreenOrientationLockPortrait:
        attribute = Qt::WA_LockPortraitOrientation;
        break;
    case ScreenOrientationLockLandscape:
        attribute = Qt::WA_LockLandscapeOrientation;
        break;
    default:
    case ScreenOrientationAuto:
        attribute = Qt::WA_AutoOrientation;
        break;
#endif // QT_VERSION < 0x040702
    };
    setAttribute(attribute, true);
}

void MainWindow::showExpanded()
{
#if defined(Q_OS_SYMBIAN) || defined(Q_WS_SIMULATOR)
    showFullScreen();
#elif defined(Q_WS_MAEMO_5)
    showMaximized();
#else
    show();
#endif
}

void MainWindow::initStackup()
{
    // Default to contacts pane
    ui->stackedWidget->setCurrentIndex(ST_Contacts);
    ui->chatTabs->setCurrentIndex(0);

    connect(ui->widget_about, SIGNAL(accepted()), this, SLOT(aboutAccepted()));
    connect(ui->widget_ports, SIGNAL(accepted()), this, SLOT(portsAccepted()));
    connect(ui->widget_ports, SIGNAL(rejected()), this, SLOT(portsRejected()));
    connect(ui->widget_encsettings, SIGNAL(accepted()), this, SLOT(encSettingsAccepted()));
    connect(ui->widget_encsettings, SIGNAL(rejected()), this, SLOT(encSettingsRejected()));
}

//=============================================
// Contacts tree
//=============================================
void MainWindow::initContactsTree()
{
    ui->popupStack->setVisible(false);

    // Attempt to load default configuration
    g_contacts.load("contacts.xml", true);

    refreshContactsTree();

    connect(ui->tree_contacts, SIGNAL(clicked(QModelIndex)), this, SLOT(contactsClicked(QModelIndex)));
    connect(ui->tree_contacts, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(contextMenu(const QPoint &)));
    connect(ui->tree_contacts->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(contactsSelected(QItemSelection,QItemSelection)));
}

void MainWindow::refreshContactsTree()
{
    int i, j;
    ContactGroup *group = NULL;
    Contact *contact = NULL;
    QStandardItemModel *model = NULL;
    QStandardItem *root = NULL;
    QString sid;

    if (!ui || !ui->tree_contacts)
        return;

    // No data model? Create one.
    if (!ui->tree_contacts->model())
        ui->tree_contacts->setModel(new QStandardItemModel(ui->tree_contacts));

    model = (QStandardItemModel *) ui->tree_contacts->model();
    // Clear the model
    model->removeRows(0, model->rowCount());

    // Get the root and attach leaves
    root = model->invisibleRootItem();
    for (i=0; i<g_contacts.numGroups(); i++)
    {
        group = g_contacts.getGroup(i);
        if (group == NULL)
            continue;

        QStandardItem *groupitem = new QStandardItem(group->getName());

        // Compile an identification string for the group
        sid = QString("Group ") + QString::number(i) + QString(" ") + group->getName();
        // And set it as user data
        groupitem->setData(QVariant(sid));
        // And append it
        root->appendRow(groupitem);

        for (j=0; j<group->numContacts(); j++)
        {
            contact = group->getContact(j);
            if (contact == NULL)
                continue;

            QStandardItem *item = new QStandardItem(contact->nickname);
            // Compile an identification string for the contact
            sid = QString("Contact ") + QString::number(j) + QString(" ") + contact->nickname;
            // And set it as user data
            item->setData(QVariant(sid));

            groupitem->appendRow(item);
        }

        // Expand the group
        ui->tree_contacts->setExpanded(groupitem->index(), true);
    }

    // No selection model? Create one.
    if (!ui->tree_contacts->selectionModel())
        ui->tree_contacts->setSelectionModel(new QItemSelectionModel(model));
}

void MainWindow::updateContacts()
{
    refreshContactsTree();
}

void MainWindow::contactsSelected(QItemSelection selected, QItemSelection deselected)
{
    int i, j;
    QStandardItemModel *model = NULL;
    QStandardItem *item = NULL;

    model = (QStandardItemModel *) ui->tree_contacts->model();

    selected_groups.clear();
    selected_contacts.clear();

    // Append the selected contacts
    for (i=0; i<selected.length(); i++)
    {
        for (j=0; j<selected.at(i).indexes().length(); j++)
        {
            // Get the tree item from the index
            item = model->itemFromIndex(selected.at(i).indexes().at(j));

            if (item)
            {
                // Classify the item
                ClassifyItem clitem(item->data().toString());
                // Is the item a group?
                if (clitem.isGroup())
                {
                    // Set it as an active group (this is where the contacts will be added)
                    g_contacts.active_group = clitem.getIndex();

                    selected_groups.append(item);
                }
                else if (clitem.isContact())
                {
                    selected_contacts.append(item);
                }
            }
        }
    }
}

void MainWindow::clientAdded(NetClient *client)
{
//    connect(client, SIGNAL(gotMessage(QString,NetClient*)), this, SLOT(gotClientMessage(QString,NetClient*)));
}

void MainWindow::contactsClicked(QModelIndex mid)
{
}

void MainWindow::on_actionRemove_contacts_triggered()
{
    int i;
    QStandardItem *item = NULL;
    QStandardItemModel *model = NULL;

    g_log.report("MainWindow::on_actionRemove_contacts_triggered()");

    if (!ui->tree_contacts->selectionModel())
    {
        g_log.report(" Error: No selection model installed for ui->tree_contacts");
        return;
    }

    if (!ui->tree_contacts->model())
    {
        g_log.report(" Error: No model installed for ui->tree_contacts");
        return;
    }

    model = (QStandardItemModel *) ui->tree_contacts->model();

    for (i=0; i<selected_groups.length(); i++)
    {
        item = selected_groups[i];
        // Remove it from the contacts
        g_contacts.removeGroup(item->text());
        // Remove it from the tree
        item->removeRows(0, item->rowCount());
        model->takeRow(item->row());
    }

    for (i=0; i<selected_contacts.length(); i++)
    {
        item = selected_contacts[i];
        // Remove it from the contacts
        g_contacts.removeContactByName(item->text());
        // Remove it from the tree
        item->parent()->removeRow(item->row());
    }
}

void MainWindow::on_actionSave_contacts_triggered()
{
    QString path;

    path = QFileDialog::getSaveFileName(this, QString("List of contacts"), QString("contacts.xml"), QString("*.xml"));

    if (path.length() > 3)
    {
        g_contacts.save(path);
    }
}

void MainWindow::on_actionLoad_contacts_triggered()
{
    QString path;

    path = QFileDialog::getOpenFileName(this, QString("List of contacts"), QString("contacts.xml"), QString("*.xml"));

    if (path.length() > 3)
    {
        g_contacts.load(path);
        updateContacts();
    }
}

void MainWindow::contextMenu(const QPoint &p)
{
/*    if (ui->stackedWidget->currentIndex() == ST_Contacts)
    {
        QMenu *menu = new QMenu(ui->tree_contacts);
        QIcon ico_add, ico_remove;
        QIcon ico_addg, ico_removeg;

        ico_add.addFile(":/icons/add_contact");
        ico_remove.addFile(":/icons/remove_contact");
        ico_addg.addFile(":/icons/add_group");
        ico_removeg.addFile(":/icons/remove_group");

        QAction *act_add = new QAction(ico_add, QString("Add contact"), menu);
        menu->addAction(act_add);
        connect(act_add, SIGNAL(triggered()), this, SLOT(addContact()));

        QAction *act_remove = new QAction(ico_remove, QString("Remove contact"), menu);
        menu->addAction(act_remove);
        connect(act_remove, SIGNAL(triggered()), this, SLOT(removeContact()));

        menu->addSeparator();

        QAction *act_addg = new QAction(ico_addg, QString("Add group"), menu);
        menu->addAction(act_addg);
        connect(act_addg, SIGNAL(triggered()), this, SLOT(addGroup()));

        QAction *act_removeg = new QAction(ico_removeg, QString("Remove group"), menu);
        menu->addAction(act_removeg);
        connect(act_removeg, SIGNAL(triggered()), this, SLOT(removeGroup()));

        menu->exec(mapToGlobal(p));
    }*/
}

//=============================================
// Contact tree item classification
//=============================================
MainWindow::ClassifyItem::ClassifyItem(QString sid)
{
    bool ok = false;

    // Some examples:
    //   Group 0 Colleagues
    //   Contact 1 Sussch

    item_sid = sid.split(' ');

    if (item_sid.length() >= 2)
        index = item_sid.at(1).toInt(&ok);
    else
        index = -1;
}

bool MainWindow::ClassifyItem::isGroup()
{
    if (item_sid.length() <= 1)
        return false;
    return (item_sid.at(0) == "Group");
}

bool MainWindow::ClassifyItem::isContact()
{
    if (item_sid.length() <= 1)
        return false;
    return (item_sid.at(0) == "Contact");
}

int MainWindow::ClassifyItem::getIndex()
{
    return index;
}

QString MainWindow::ClassifyItem::getName()
{
    if (item_sid.length() <= 2)
        return QString("");
    return item_sid.at(2);
}

//=============================================
// Add contact popup
//=============================================
void MainWindow::on_actionAdd_contact_triggered()
{
    addContact();
}

void MainWindow::addContact()
{
    ui->widget_addContact->show();
    connect(ui->widget_addContact, SIGNAL(accepted()), this, SLOT(addContactAccepted()));
    connect(ui->widget_addContact, SIGNAL(rejected()), this, SLOT(addContactRejected()));

    ui->popupStack->setCurrentIndex(0);
    ui->popupStack->setVisible(true);
}

void MainWindow::addContactAccepted()
{
    ui->popupStack->setVisible(false);

    updateContacts();
}

void MainWindow::addContactRejected()
{
    ui->popupStack->setVisible(false);
}

void MainWindow::editContact()
{
    ui->widget_editContact->show();
    connect(ui->widget_editContact, SIGNAL(accepted()), this, SLOT(addContactAccepted()));
    connect(ui->widget_editContact, SIGNAL(rejected()), this, SLOT(addContactRejected()));

    ui->popupStack->setCurrentIndex(2);
    ui->popupStack->setVisible(true);
}

void MainWindow::on_actionEdit_contact_triggered()
{
    Contact *contact = NULL;
    QString name;

    if (selected_contacts.isEmpty())
        return;

    name = selected_contacts.at(0)->text();
    contact = g_contacts.findContactByName(name);

    if (contact)
    {
        ui->widget_editContact->setContact(contact);

        editContact();
    }
}

//=============================================
// Add group popup
//=============================================
void MainWindow::on_actionAdd_group_triggered()
{
    addGroup();
}

void MainWindow::addGroup()
{
    ui->widget_addGroup->show();
    connect(ui->widget_addGroup, SIGNAL(accepted()), this, SLOT(addGroupAccepted()));
    connect(ui->widget_addGroup, SIGNAL(rejected()), this, SLOT(addGroupRejected()));

    ui->popupStack->setCurrentIndex(1);
    ui->popupStack->setVisible(true);
}

void MainWindow::addGroupAccepted()
{
    ui->popupStack->setVisible(false);

    updateContacts();
}

void MainWindow::addGroupRejected()
{
    ui->popupStack->setVisible(false);
}

//=============================================
// Networking
//=============================================
void MainWindow::initNetwork()
{
    g_network.start();
    // We need notification of any clients in order to connect to their signals.
    connect(&g_network, SIGNAL(clientAdded(NetClient*)), this, SLOT(clientAdded(NetClient*)));
    connect(&g_network, SIGNAL(serverGotPacket(Packet,NetSubclient*)), this, SLOT(serverGotPacket(Packet,NetSubclient*)));
    connect(&g_network, SIGNAL(clientGotPacket(Packet,NetClient*)), this, SLOT(clientGotPacket(Packet,NetClient*)));
    connect(&g_network, SIGNAL(connectionAccepted(NetSubclient*)), this, SLOT(connectionAccepted(NetSubclient*)));
    connect(&g_network, SIGNAL(connectionLost(NetClient*)), this, SLOT(connectionLost(NetClient*)));
    connect(&g_network, SIGNAL(gotConnected(NetClient*)), this, SLOT(gotConnected(NetClient*)));

    connect(ui->widget_fileshare, SIGNAL(acceptFile(QString,QString)), this, SLOT(acceptFile(QString,QString)));
}

//=============================================
// Chat window general
//=============================================
void MainWindow::initChat()
{
    ui->popupChatStack->setVisible(false);

    ui->editLog->setDocument(&chat_log);

    // Install a text filter that calls sendMessage on Return
    connect(&text_filter, SIGNAL(sendText()), this, SLOT(sendMessage()));
    ui->editSend->installEventFilter(&text_filter);

    // Attempt to load default encryption config
    ui->widget_encsettings->loadSettings("encryption.xml", true);
}

void MainWindow::on_actionStart_a_conversation_triggered()
{
    ui->stackedWidget->setCurrentIndex(ST_Contacts);
    ui->chatTabs->setCurrentIndex(1);
    startChat();
}

void MainWindow::startChat()
{
    ui->editSend->setFocus();

    // Make all contacts active
    active_contacts.clear();
    for (int i=0; i<g_contacts.numContacts(); i++)
    {
        active_contacts.append(g_contacts.getContact(i));
    }
}

void MainWindow::on_chatTabs_currentChanged(int index)
{
    if (index == 1)
        startChat();
}

//=============================================
// Messaging
//=============================================
void MainWindow::sendMessage()
{
    QStringList targets;
    QString message;
    Packet packet;
    Contact *contact = NULL;

    g_log.report("MainWindow::sendMessage()");

    //! \todo Send HTML instead of plaintext?

    packet.setText(ui->editSend->toPlainText());
    packet.setType(Packet::PT_TEXT);

    // See if there are any specific target names given
    targets = parseTargetNames(message);

    // Any?
    if (!targets.empty())
    {
        g_log.report(QString(" to %1 targets:").arg(targets.length()));
        for (int i=0; i<targets.length(); i++)
        {
            g_log.report(QString("  %1. \"%2\"").arg(i + 1).arg(targets.at(i)));

            contact = g_contacts.findContactByName(targets.at(i));
            if (contact)
            {
                g_log.report(QString("   (contact at \"%1\")").arg(contact->ip_address));
                g_network.postPacket(contact->ip_address, packet);
            }
        }
    }
    // No specific target names?
    else
    {
        // Send to all participants
        for (int i=0; i<active_contacts.length(); i++)
        {
            contact = active_contacts.at(i);
            if (contact)
            {
                g_log.report(QString(" to \"%1\" at %2").arg(contact->nickname).arg(contact->ip_address));
                g_network.postPacket(contact->ip_address, packet);
            }
        }
    }

    appendToLog(formatMessage(QString("You"), QString("#336600"), packet.getText()));

    // Clear the textbox
    ui->editSend->clear();
    // Sets cursor at the beginning of the textbox
    QTextCursor cursor = ui->editSend->textCursor();
    cursor.movePosition(QTextCursor::Start);
    ui->editSend->setTextCursor(cursor);
}

void MainWindow::serverGotPacket(Packet packet, NetSubclient *subclient)
{
    QString color = "#FF6600";
    QString nickname("Unknown");
    QString addr("???.???.???.???");

    if (subclient && subclient->contact)
    {
        color = "#3366FF";
        nickname = subclient->contact->nickname;
        addr = subclient->contact->ip_address;
    }

    appendToLog(formatMessage(nickname, color, QString(packet.data)));
    g_log.report(QString("MainWindow::serverGotPacket()"));
    g_log.report(QString(" Contact \"%1\" at %2").arg(nickname).arg(addr));
    g_log.report(QString(" Type: %1").arg(packet.packet_type));
    g_log.report(QString(" Timestamp %1").arg(packet.timestamp));
}

void MainWindow::clientGotPacket(Packet packet, NetClient *client)
{
    QString color = "#FF6600";
    QString nickname("Unknown");
    QString addr("???.???.???.???");

    //! \bug A memory-corrupting crashbug here somewhere?

    g_log.report(QString("MainWindow::clientGotPacket()"));

    if (!client)
    {
        g_log.report(" Error: Client has a NULL pointer");
        return;
    }

    if (client && client->target_contact)
    {
        color = "#3366FF";
        nickname = client->target_contact->nickname;
        addr = client->target_contact->ip_address;
    }

    g_log.report(QString(" Contact \"%1\" at %2").arg(nickname).arg(addr));
    g_log.report(QString(" Timestamp %1").arg(packet.timestamp));
    g_log.report(QString(" Type: %1").arg(packet.packet_type));
    g_log.report(QString(" Data length: %1").arg(packet.data_length));

    if (packet.packet_type == Packet::PT_TEXT)
    {
        appendToLog(formatMessage(nickname, color, QString(packet.data)));
    }
    else if (packet.packet_type == Packet::PT_NOTE_SHARE_FILE)
    {
        appendToLog(formatMessage(nickname, color, QString("[Sharing file \"%1\"]").arg(QString(packet.data))));

        ui->popupChatStack->setCurrentIndex(0);
        ui->popupChatStack->setVisible(true);
        ui->widget_fileshare->show();

        ui->widget_fileshare->addFile(QString(packet.data), client->getTargetName());
    }
    else if (packet.packet_type == Packet::PT_NOTE_ACCEPT_FILE)
    {
        appendToLog(formatMessage(nickname, color, QString("[Accepting file \"%1\"]").arg(QString(packet.data))));

        ui->popupChatStack->setCurrentIndex(0);
        ui->popupChatStack->setVisible(true);
        ui->widget_fileshare->show();

        SharedFile *sf = getUploadingFileByName(QString(packet.data));
        if (!sf)
        {
            g_log.report(QString(" Error: Contact \"%1\" at %2 is trying to accept a file (\"%3\") that has not been shared")
                         .arg(nickname)
                         .arg(addr)
                         .arg(QString(packet.data)));
            return;
        }

        sf->source_contact = client->target_contact;

        // Start sending
        sendFile(client->getTargetIP(), QString(packet.data));
    }
    else if (packet.packet_type == Packet::PT_FILE)
    {
        appendToLog(formatMessage(nickname, color, QString("[Got %1 Bytes of the file]")
                                  .arg(packet.data.length())));

        ui->popupChatStack->setCurrentIndex(0);
        ui->popupChatStack->setVisible(true);
        ui->widget_fileshare->show();

        SharedFile *sf = getDownloadingFileByAddr(addr);

        if (!sf)
        {
            g_log.report(QString(" Error: Contact \"%1\" at %2 is sending a file without request")
                         .arg(nickname)
                         .arg(addr));
            return;
        }

        // Generate a filename
        QString fname = QString("downloads/file_%1_%2.part")
                .arg(sf->name)
                .arg(packet.packet_index, 7, 10, QLatin1Char('0'));

        g_log.report(QString(" Got %1 Bytes of file \"%2\". Saved it to \"%3\"")
                     .arg(packet.data.length())
                     .arg(sf->name)
                     .arg(fname));

        sf->temp_paths.append(fname);

        // Save the packet
        QFile file(fname);
        if (!file.open(QIODevice::WriteOnly))
        {
            g_log.report(QString(" Error: Failed to open file \"%1\" for writing")
                         .arg(fname));
            return;
        }

        file.write(packet.data);
        file.close();
    }
    else if (packet.packet_type == Packet::PT_NOTE_END_FILE)
    {
        SharedFile *sf = getDownloadingFileByName(packet.getText());

        if (!sf)
        {
            g_log.report(QString(" Error: Contact \"%1\" at %2 finished sending a file without request")
                         .arg(nickname)
                         .arg(addr));
            return;
        }

        QString path;
        path = QFileDialog::getSaveFileName(this, QString("Save the file"), packet.getText(), QString("*.*"));

        if (path.length() > 0)
        {
            sf->merge(path);
            sf->clear();
            files_downloading.clear();
        }
    }
}

QString MainWindow::formatMessage(QString whosays, QString color, QString message)
{
    // Remove line endings
    message.replace(QString("\r\n"), QString(""));

    return QString("<p><span style=\"font-weight: bold; color: ") + color + QString(";\">") + whosays + QString(":</span> ") + message + QString("</p>");
}

QStringList MainWindow::parseTargetNames(QString &message)
{
    QStringList strl;

    // For example:
    //    @Sussch, Ted, Paul: Let's discuss the interfaces.

    // Is this message directed to specific targets?
    if (message[0].cell() == QChar('@'))
    {
        // Cut the @ character
        message = message.right(message.length() - 1);
        // Only take up to the first colon
        strl = message.split(QString(":"));

        if (strl.length() >= 2)
        {
            // Message is now the rest of it
            message = strl[1];
            // Return all names separated with commas or whitespaces
            return strl[0].split(QRegExp(QString("[, ]")));
        }
    }
    return strl;
}

void MainWindow::appendToLog(QString text)
{
    QTextCursor cur;

    chat_log.setHtml(chat_log.toHtml() + text);

    // Autoscroll
    cur = ui->editLog->textCursor();
    cur.movePosition(QTextCursor::End);
    ui->editLog->setTextCursor(cur);
}

bool MainWindow::saveLog(QString path)
{
    // Open the file for writing
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly))
    {
        g_log.report(QString("MainWindow::saveLog()"));
        g_log.report(QString(" Error: Failed to write to file \"%1\"").arg(path));
        return false;
    }

    // Write to file
    QTextStream stream(&file);
    stream << chat_log.toHtml();

    file.close();
    return true;
}

void MainWindow::on_actionSave_conversation_triggered()
{
    QString path;

    path = QFileDialog::getSaveFileName(this, QString("List of contacts"), QString(), QString("*.html"));

    if (path.length() > 3)
    {
        saveLog(path);
    }
}

void MainWindow::on_actionAbout_triggered()
{
    stackwidget_id = ui->stackedWidget->currentIndex();

    ui->stackedWidget->setCurrentIndex(ST_About);
    ui->widget_about->show();
}

void MainWindow::aboutAccepted()
{
    ui->stackedWidget->setCurrentIndex(stackwidget_id);
}

void MainWindow::on_actionPort_config_triggered()
{
    stackwidget_id = ui->stackedWidget->currentIndex();

    ui->stackedWidget->setCurrentIndex(ST_Ports);
    ui->widget_ports->show();
}

void MainWindow::portsAccepted()
{
    ui->stackedWidget->setCurrentIndex(stackwidget_id);
}

void MainWindow::portsRejected()
{
    ui->stackedWidget->setCurrentIndex(stackwidget_id);
}

void MainWindow::on_actionEncryption_config_triggered()
{
    stackwidget_id = ui->stackedWidget->currentIndex();

    ui->stackedWidget->setCurrentIndex(ST_EncryptionSettings);
    ui->widget_encsettings->show();
}

void MainWindow::encSettingsAccepted()
{
    ui->stackedWidget->setCurrentIndex(stackwidget_id);
}

void MainWindow::encSettingsRejected()
{
    ui->stackedWidget->setCurrentIndex(stackwidget_id);
}

void MainWindow::on_btnAddGroup_clicked()
{
    addGroup();
}

void MainWindow::on_btnAddContact_clicked()
{
    addContact();
}

void MainWindow::on_btnSendFile_clicked()
{
    QString path;

    path = QFileDialog::getOpenFileName(this, QString("Open file"), QString(), QString("*.*"));

    if (path.length() > 0)
    {
        startFileshare(path);
    }
}

void MainWindow::startFileshare(QString path)
{
    QStringList tokens;
    SharedFile sf;
    Contact *contact = NULL;
    Packet packet;

    g_log.report("MainWindow::startFileshare()");

    sf.path = path;
    tokens = path.split(QRegExp(QString("[\\/]")), QString::SkipEmptyParts);

    if (!tokens.isEmpty())
        sf.name = tokens.last();

    packet.setText(sf.name);
    packet.setType(Packet::PT_NOTE_SHARE_FILE);

    files_uploading.append(sf);

    // Send to all participants
    for (int i=0; i<active_contacts.length(); i++)
    {
        contact = active_contacts.at(i);
        if (contact)
        {
            g_log.report(QString(" to \"%1\" at %2").arg(contact->nickname).arg(contact->ip_address));
            g_network.postPacket(contact->ip_address, packet);
        }
    }

    appendToLog(formatMessage(QString("You"), QString("#336600"), QString("[Sharing file \"%1\"]").arg(sf.name)));
}

void MainWindow::acceptFile(QString filename, QString sender)
{
    Contact *contact = NULL;
    SharedFile sf;
    Packet packet;

    g_log.report("MainWindow::acceptFile()");

    ui->popupChatStack->setVisible(false);

    // Check if the downloads directory exists
    // If not, then force it to exist =P
    QDir dir(QString("downloads"));
    if (!dir.exists())
    {
        g_log.report(QString(" Directory \"%1\" doesn't exist yet. Making it.")
                     .arg(dir.absolutePath()));
        dir.mkdir(dir.absolutePath());
    }

    sf.name = filename;
    sf.source_contact = g_contacts.findContactByName(sender);
    files_downloading.append(sf);

    packet.setText(filename);
    packet.setType(Packet::PT_NOTE_ACCEPT_FILE);

    // Send to all participants
    for (int i=0; i<active_contacts.length(); i++)
    {
        contact = active_contacts.at(i);
        if (contact && contact->nickname == sender)
        {
            g_log.report(QString(" to \"%1\" at %2").arg(sender).arg(contact->ip_address));
            g_network.postPacket(contact->ip_address, packet);
        }
    }

    appendToLog(formatMessage(QString("You"),
                              QString("#336600"),
                              QString("[Accepting file \"%1\" from %2]")
                              .arg(filename)
                              .arg(sender)));
    g_log.report(QString(" Accepting file \"%1\" from %2").arg(filename).arg(sender));
}

void MainWindow::sendFile(QString targetIP, QString filename)
{
    int bufsize = 0;
    SharedFile *sf = NULL;
    Contact *contact = NULL;
    QString targetName("Unknown");
    Packet packet;

    g_log.report("MainWindow::sendFile()");

    sf = getUploadingFileByAddr(targetIP);
    if (!sf)
    {
        g_log.report(" Error: Attempting to send a file without having received a notification first");
        return;
    }

    // Load the file to be sent
    QFile file(sf->path);
    if (!file.open(QIODevice::ReadOnly))
    {
        g_log.report(QString(" Error: Failed to read file \"%1\"").arg(sf->path));
        file.close();
        return;
    }

    // Send to the target
    contact = g_contacts.findContactByAddress(targetIP);
    if (contact)
    {
        targetName = contact->nickname;
    }

    // Send by 1024 - HeaderOverhead bytes
    bufsize = 1024 - Packet::getOverhead();
    for (qint64 i=0, j=0; i<file.size(); i+=bufsize)
    {
        packet.clear();
        packet.data = file.read(bufsize);
        packet.setType(Packet::PT_FILE);
        packet.setIndex(j++);
        packet.setCurrentTimestamp();

        g_network.postPacket(targetIP, packet);
    }

    // Post EOF
    packet.clear();
    //! \todo Add CRC
    packet.setText(filename);
    packet.setType(Packet::PT_NOTE_END_FILE);
    packet.setCurrentTimestamp();
    g_network.postPacket(targetIP, packet);

    appendToLog(formatMessage(QString("You"),
                              QString("#336600"),
                              QString("[Sent file \"%1\" (%2 Bytes)]")
                              .arg(sf->name)
                              .arg(file.size())));

    file.close();

    ui->widget_fileshare->removeFile(sf->name);
    sf->clear();
    files_uploading.clear();
}

SharedFile *MainWindow::getDownloadingFileByAddr(QString sourceIP)
{
    SharedFile *sf = NULL;
    Contact *contact = NULL;

    for (int i=0; i<files_downloading.length(); i++)
    {
        contact = files_downloading[i].source_contact;
        if (contact && contact->ip_address == sourceIP)
        {
            sf = &files_downloading[i];
            break;
        }
    }

    return sf;
}

SharedFile *MainWindow::getDownloadingFileByName(QString fname)
{
    SharedFile *sf = NULL;

    for (int i=0; i<files_downloading.length(); i++)
    {
        if (files_downloading[i].name == fname)
        {
            sf = &files_downloading[i];
            break;
        }
    }

    return sf;
}

SharedFile *MainWindow::getUploadingFileByAddr(QString sourceIP)
{
    SharedFile *sf = NULL;
    Contact *contact = NULL;

    for (int i=0; i<files_uploading.length(); i++)
    {
        contact = files_uploading[i].source_contact;
        if (contact && contact->ip_address == sourceIP)
        {
            sf = &files_uploading[i];
            break;
        }
    }

    return sf;
}

SharedFile *MainWindow::getUploadingFileByName(QString fname)
{
    SharedFile *sf = NULL;

    for (int i=0; i<files_uploading.length(); i++)
    {
        if (files_uploading[i].name == fname)
        {
            sf = &files_uploading[i];
            break;
        }
    }

    return sf;
}

void MainWindow::connectionAccepted(NetSubclient *subclient)
{
    QString color = "#FF6600";
    QString nickname("Unknown");
    QString addr("???.???.???.???");

    if (subclient && subclient->contact)
    {
        color = "#3366FF";
        nickname = subclient->contact->nickname;
        addr = subclient->contact->ip_address;
    }

    QString message;
    message = formatMessage(nickname, color, QString("[Connected to subclient at %1]").arg(addr));
    appendToLog(message);
}

void MainWindow::connectionLost(NetClient *client)
{
    QString color = "#FF6600";
    QString nickname("Unknown");
    QString addr("???.???.???.???");

    if (client && client->target_contact)
    {
        color = "#3366FF";
        nickname = client->target_contact->nickname;
        addr = client->target_contact->ip_address;
    }

    QString message;
    message = formatMessage(nickname, color, QString("[Lost connection to server at %1]").arg(addr));
    appendToLog(message);
}

void MainWindow::gotConnected(NetClient *client)
{
    QString color = "#FF6600";
    QString nickname("Unknown");
    QString addr("???.???.???.???");

    if (client && client->target_contact)
    {
        color = "#3366FF";
        nickname = client->target_contact->nickname;
        addr = client->target_contact->ip_address;
    }

    QString message;
    message = formatMessage(nickname, color, QString("[Connected to server at %1]").arg(addr));
    appendToLog(message);
}
