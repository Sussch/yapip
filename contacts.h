#ifndef CONTACTS_H
#define CONTACTS_H

#include "contactgroup.h"

class Contacts
{
public:
    Contacts();

    /**
     * Resets to default groups
     */
    void addDefaultGroups();

    /**
     * Adds a group to the contacts
     */
    bool addGroup(ContactGroup group);
    /**
     * Removes a group from the contacts
     */
    bool removeGroup(QString groupname);
    /**
     * Clears the contacts
     */
    void removeAllGroups();
    /**
     * Checks if there's a certain group in the contacts
     */
    bool hasGroup(QString groupname) const;

    /**
     * Finds a contact by name
     * @return NULL when not found
     */
    Contact *findContactByName(QString nickname);
    /**
     * Finds a contact by IP address
     * @return NULL when not found
     */
    Contact *findContactByAddress(QString IPaddress);
    /**
     * Removes a contact by name
     * @return True on success, otherwise false
     */
    bool removeContactByName(QString nickname);
    /**
     * Removes a contact by IP address
     * @return True on success, otherwise false
     */
    bool removeContactByAddress(QString IPaddress);

    /**
     * Finds a group by name
     * @param[in] nickname Group name
     * @return Pointer to the group or NULL if not found
     */
    ContactGroup *findGroupByName(QString nickname);

    /**
     * Gets the number of groups in the contacts
     */
    int numGroups() const;
    /**
     * Gets a group by index
     * @return NULL when out of range
     */
    ContactGroup *getGroup(int index);

    /**
     * Gets the number of contacts
     */
    int numContacts() const;
    /**
     * Gets a contact by index
     * @return NULL when out of range
     */
    Contact *getContact(int index);

    /**
     * Saves the list of contacts
     */
    bool save(QString path);
    /**
     * Loads the list of contacts
     */
    bool load(QString path, bool quiet = false);

    int active_group;

private:
    //! List of contact groups
    QList<ContactGroup> groups;
};

//! A global instance of the contacts book
extern Contacts g_contacts;

#endif // CONTACTS_H
