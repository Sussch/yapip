#include "filesharewidget.h"
#include "ui_filesharewidget.h"
#include "log.h"

#include <QStandardItemModel>
#include <QPushButton>

FileShareWidget::FileShareWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FileShareWidget)
{
    ui->setupUi(this);

    init();

    hide();
}

FileShareWidget::~FileShareWidget()
{
    delete ui;
}

void FileShareWidget::show()
{
    QWidget::show();
}

void FileShareWidget::on_btnClose_clicked()
{
    emit accepted();
    hide();
}

void FileShareWidget::addFile(QString name, QString sender)
{
    QStandardItemModel *model = NULL;
    QPushButton *button = NULL;
    QModelIndex index;
    int row;

    model = (QStandardItemModel *) ui->listFiles->model();

    model->appendRow(new QStandardItem(sender));

    row = model->rowCount() - 1;

    model->setItem(row, 1, new QStandardItem(name));

    index = model->index(row, 2);

    button = new QPushButton(QString("v"));
    connect(button, SIGNAL(clicked()), &accept_signal_mapper, SLOT(map()));
    accept_signal_mapper.setMapping(button, name);
    ui->listFiles->setIndexWidget(index, button);

    index = model->index(row, 3);

    button = new QPushButton(QString("x"));
    connect(button, SIGNAL(clicked()), &reject_signal_mapper, SLOT(map()));
    reject_signal_mapper.setMapping(button, name);
    ui->listFiles->setIndexWidget(index, button);
}

void FileShareWidget::removeFile(QString name)
{
    QStandardItemModel *model = NULL;

    model = (QStandardItemModel *) ui->listFiles->model();

    // Remove the file
    for (int i=0; i<model->rowCount(); i++)
    {
        if (model->item(i, 1)->text() == name)
        {
            ui->listFiles->model()->removeRow(i);
            break;
        }
    }
}

void FileShareWidget::init()
{
    QStandardItemModel *model = new QStandardItemModel(0, 4, this);

    model->setHeaderData(0, Qt::Horizontal, QObject::tr("Contact"));
    model->setHeaderData(1, Qt::Horizontal, QObject::tr("Shared files"));
    model->setHeaderData(2, Qt::Horizontal, QObject::tr(""));
    model->setHeaderData(3, Qt::Horizontal, QObject::tr(""));

    ui->listFiles->setModel(model);
    ui->listFiles->setColumnWidth(2, 20);
    ui->listFiles->setColumnWidth(3, 20);

    connect(&accept_signal_mapper, SIGNAL(mapped(QString)), this, SLOT(fileAccepted(QString)));
    connect(&reject_signal_mapper, SIGNAL(mapped(QString)), this, SLOT(fileRejected(QString)));
}

void FileShareWidget::fileAccepted(QString filename)
{
    QStandardItemModel *model = NULL;

    model = (QStandardItemModel *) ui->listFiles->model();

    // Remove the file
    for (int i=0; i<model->rowCount(); i++)
    {
        if (model->item(i, 1)->text() == filename)
        {
            g_log.report(QString("User accepted file \"%1\"").arg(filename));
            emit acceptFile(filename, model->item(i, 0)->text());
            return;
        }
    }
}

void FileShareWidget::fileRejected(QString filename)
{
    removeFile(filename);
}
