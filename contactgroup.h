#ifndef CONTACTGROUP_H
#define CONTACTGROUP_H

#include <QtCore>
#include <QList>
#include <QString>
#include "contact.h"

class ContactGroup
{
public:
    ContactGroup();
    ContactGroup(QString groupname);

    ~ContactGroup();

    /**
     * Adds a contact to the group or updates the fields of an old contact.
     * @return True if anything changed, otherwise false
     */
    bool addContact(Contact contact);
    /**
     * Removes a contact from the group
     * @param[in] IPaddr IP address of the contact to be removed
     */
    bool removeContact(QString IPaddr);
    /**
     * Removes a contact by index
     * @param[in] index Contact index
     * @return True on success, false on failure
     */
    bool removeContact(int index);
    /**
     * Clears the group
     */
    void removeAllContacts();
    /**
     * Checks if the group contains a contact by IP address
     */
    bool hasContact(QString IPaddr) const;

    /**
     * Gets the number of contacts in this group
     */
    int numContacts() const;
    /**
     * Gets a contact by its index
     * @return NULL when contact is not found
     */
    Contact *getContact(int index);
    /**
     * Gets a contact by its address
     * @return NULL when contact is not found
     */
    Contact *getContact(QString IPaddr);

    /**
     * Gets the group name
     */
    QString getName() const;
    /**
     * Renames the group
     */
    void setName(QString newname);

private:
    //! Group name
    QString name;
    //! List of contacts in this group
    QList<Contact> contacts;
};

#endif // CONTACTGROUP_H
