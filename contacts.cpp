#include "contacts.h"

#include <QtXml/QDomDocument>
#include <QMessageBox>

#include "netinterface.h"
#include "log.h"

#include <iostream>

Contacts g_contacts;

Contacts::Contacts()
{
    addDefaultGroups();
    active_group = 0;
}

void Contacts::addDefaultGroups() {
    ContactGroup other("Other contacts");
    addGroup(other);
}

bool Contacts::addGroup(ContactGroup group)
{
    if (hasGroup(group.getName()))
        return false;

    groups.append(group);
    return true;
}

bool Contacts::removeGroup(QString groupname)
{
    for (int i=0; i<groups.length(); i++)
    {
        if (groups.at(i).getName() == groupname) {
            groups.removeAt(i);
            return true;
        }
    }
    return false;
}

void Contacts::removeAllGroups()
{
    groups.clear();
}

bool Contacts::hasGroup(QString groupname) const
{
    for (int i=0; i<groups.length(); i++)
    {
        if (groups.at(i).getName() == groupname)
            return true;
    }
    return false;
}

int Contacts::numGroups() const
{
    return groups.length();
}

ContactGroup *Contacts::getGroup(int index)
{
    if (index >= 0 && index < groups.length())
        return &groups[index];
    return NULL;
}

Contact *Contacts::findContactByName(QString nickname)
{
    int i, j;
    ContactGroup *group = NULL;
    Contact *contact = NULL;

    for (i=0; i<groups.length(); i++)
    {
        group = &groups[i];
        for (j=0; j<group->numContacts(); j++)
        {
            contact = group->getContact(j);
            if (!contact)
                continue;
            if (contact->nickname == nickname)
                return contact;
        }
    }

    return NULL;
}

Contact *Contacts::findContactByAddress(QString IPaddress)
{
    int i, j;
    ContactGroup *group = NULL;
    Contact *contact = NULL;

    for (i=0; i<groups.length(); i++)
    {
        group = &groups[i];
        for (j=0; j<group->numContacts(); j++)
        {
            contact = group->getContact(j);
            if (!contact)
                continue;
            if (contact->ip_address == IPaddress)
                return contact;
        }
    }

    return NULL;
}

bool Contacts::removeContactByName(QString nickname)
{
    int i, j;
    ContactGroup *group = NULL;
    Contact *contact = NULL;

    for (i=0; i<groups.length(); i++)
    {
        group = &groups[i];
        for (j=0; j<group->numContacts(); j++)
        {
            contact = group->getContact(j);
            if (!contact)
                continue;
            if (contact->nickname == nickname)
            {
                g_network.removeClient(contact->ip_address);
                group->removeContact(j);
                return true;
            }
        }
    }

    return false;
}

bool Contacts::removeContactByAddress(QString IPAddress)
{
    int i, j;
    ContactGroup *group = NULL;
    Contact *contact = NULL;

    for (i=0; i<groups.length(); i++)
    {
        group = &groups[i];
        for (j=0; j<group->numContacts(); j++)
        {
            contact = group->getContact(j);
            if (!contact)
                continue;
            if (contact->ip_address == IPAddress)
            {
                g_network.removeClient(IPAddress);
                group->removeContact(j);
                return true;
            }
        }
    }

    return false;
}

int Contacts::numContacts() const
{
    int sum = 0;

    for (int i=0; i<groups.length(); i++)
    {
        sum += groups.at(i).numContacts();
    }
    return sum;
}

Contact *Contacts::getContact(int index)
{
    int sum = 0;
    ContactGroup *group = NULL;

    for (int i=0; i<groups.length(); i++)
    {
        group = &groups[i];

        sum += group->numContacts();

        if (sum > index)
        {
            sum -= group->numContacts();
            return group->getContact(index - sum);
        }
    }

    return NULL;
}

ContactGroup *Contacts::findGroupByName(QString nickname)
{
    for (int i=0; i<groups.length(); i++)
    {
        if (groups.at(i).getName() == nickname)
            return &groups[i];
    }

    return NULL;
}

bool Contacts::save(QString path)
{
    int i, j;

    g_log.report("Contacts::save()");

    ContactGroup *pg = NULL;
    Contact *pc = NULL;
    QDomDocument document("contactlists");
    QDomElement root = document.createElement("contactlist");

    document.appendChild(root);

    // For every group
    for (i=0; i<groups.length(); i++)
    {
        QDomElement group = document.createElement(QString("group"));
        pg = &groups[i];
        // Set its name
        group.setAttribute("name", pg->getName());
        // Append it
        root.appendChild(group);

        // For every contact
        for (j=0; j<pg->numContacts(); j++)
        {
            QDomElement contact = document.createElement(QString("contact"));
            pc = pg->getContact(j);
            // Set its name
            contact.setAttribute("name", pc->nickname);
            // Set its IP
            contact.setAttribute("IP", pc->ip_address);
            // Append it
            group.appendChild(contact);
        }
    }

    // Open the file for writing
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly))
    {
        QString msg(QString("Failed to write to file \"%1\"").arg(path));
        g_log.report(QString(" Error: %1").arg(msg));
        QMessageBox::critical(NULL, "File error", msg);
        return false;
    }

    // Write to file
    QTextStream stream(&file);
    stream << document.toString();

    file.close();

    return true;
}

bool Contacts::load(QString path, bool quiet)
{
    QString error_msg;
    int error_line, error_column;

    g_log.report("Contacts::load()");

    QDomDocument document(QString("contactlist"));
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly))
    {
        if (quiet)
        {
            g_log.report(QString(" Contacts file \"%1\" doesn't exist yet")
                         .arg(path));
        }
        else
        {
            QString msg(QString("Failed to read from file \"%1\"").arg(path));
            g_log.report(QString(" Error: %1").arg(msg));
            QMessageBox::critical(NULL, "File error", msg);
        }
        return false;
    }

    if (!document.setContent(&file, &error_msg, &error_line, &error_column))
    {
        g_log.report(QString(" XML Syntax error: %1, line %2, column %3:")
                     .arg(path)
                     .arg(error_line)
                     .arg(error_column));
        g_log.report(QString("  %1")
                     .arg(error_msg));

        if (!quiet)
        {
            QMessageBox::critical(NULL, QString("XML error"),
                                  QString("Syntax error in the contacts file \"%1\".\r\nSee the log for details.")
                                  .arg(path));
        }

        file.close();
        return false;
    }

    // The DOM tree is now in memory
    file.close();

    // Is this a correct contactlist file?
    QDomElement root = document.documentElement();
    if (root.tagName() != "contactlist")
    {
        QString msg(QString("The file \"%1\" does not contain a list of contacts.")
                    .arg(path));
        g_log.report(QString(" Error: %1").arg(msg));

        if (!quiet)
        {
            QMessageBox::critical(NULL, QString("XML error"), msg);
        }

        return false;
    }

    // Remove all previous groups and contacts
    //removeAllGroups();

    // For each element
    QDomElement element = root.firstChildElement();
    while (!element.isNull())
    {
        // Find groups
        if (element.tagName() == "group")
        {
            ContactGroup group;
            ContactGroup *group_ptr = NULL;

            group.setName(element.attribute("name", ""));
            addGroup(group);

            group_ptr = findGroupByName(group.getName());
            if (!group_ptr)
            {
                g_log.report(QString(" Error: Added group \"%1\", but failed to find it. Bailing out.")
                             .arg(group.getName()));
            }
            else
            {
                // And their subelements (contacts)
                QDomElement subelement = element.firstChildElement();
                while(!subelement.isNull())
                {
                    if (subelement.tagName() == "contact")
                    {
                        Contact contact;

                        contact.nickname = subelement.attribute("name", "");
                        contact.ip_address = subelement.attribute("IP", "");
                        group_ptr->addContact(contact);
                        // Add a client for this contact
                        g_network.addClient(groups.last().getContact(groups.last().numContacts() - 1));
                    }

                    subelement = subelement.nextSiblingElement();
                }
            }
        }

        element = element.nextSiblingElement();
    }

    return true;
}
