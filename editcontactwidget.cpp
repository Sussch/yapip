#include "editcontactwidget.h"
#include "ui_editcontactwidget.h"

#include "contacts.h"
#include "netinterface.h"

EditContactWidget::EditContactWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditContactWidget)
{
    ui->setupUi(this);

    connect(ui->editIP, SIGNAL(returnPressed()), this, SLOT(on_btnApply_clicked()));

    hide();
}

EditContactWidget::~EditContactWidget()
{
    delete ui;
}

void EditContactWidget::show()
{
    ui->editNick->setFocus();

    QWidget::show();
}

void EditContactWidget::on_btnApply_clicked()
{
    contact->nickname = ui->editNick->text();
    contact->ip_address = ui->editIP->text();

    NetClient *pclient = g_network.findClient(contact->ip_address);

    if (pclient)
    {
        pclient->socket.close();
        pclient->connectToTarget();
    }
    else
    {
        // Add a client for this contact
        g_network.addClient(contact);
    }

    emit accepted();
    hide();
}

void EditContactWidget::on_btnCancel_clicked()
{
    emit rejected();
    hide();
}

void EditContactWidget::setContact(Contact *p_contact)
{
    contact = p_contact;

    if (contact)
    {
        ui->editNick->setText(contact->nickname);
        ui->editIP->setText(contact->ip_address);
    }
}
