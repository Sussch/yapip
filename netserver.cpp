#include "netserver.h"

#include <QMessageBox>
#include <iostream>

#include "log.h"
#include "contacts.h"
#include "portconfig.h"
#include "packet.h"

NetServer::NetServer(QObject *parent) :
    QObject(parent)
{
}

NetServer::~NetServer()
{
    clear();
}

void NetServer::clear()
{
    for (int i=0; i<subclients.length(); i++)
    {
        kickSubclient(i);
    }
    subclients.clear();
}

void NetServer::start()
{
    int port = -1;

    g_log.report("NetServer::start()");

    connect(&server, SIGNAL(newConnection()), this, SLOT(acceptConnection()));

    do {
        port = portconfig.nextPort();

        if (port < 0)
            break;

        // Make sure we won't try the same port again
        portconfig.markUsed(port);

        if (server.listen(QHostAddress::Any, port))
        {
            g_log.report(QString(" Server now listening on port %1").arg(port));
            break;
        }
        else
        {
            g_log.report(QString(" Server failed to listen on port %1").arg(port));
        }
    } while(true);

    connect(&ping_timer, SIGNAL(timeout()), this, SLOT(pingTimerEvent()));

    ping_timer.setInterval(3000);
    ping_timer.start();
}

void NetServer::restart()
{
    int port = -1;

    ping_timer.stop();

    //! \todo Test if newConnection() is triggered. Does it have to be connected again?

    g_log.report("NetServer::restart()");

    server.close();

    do {
        port = portconfig.nextPort();

        if (port < 0)
            break;

        // Make sure we won't try the same port again
        portconfig.markUsed(port);

        if (server.listen(QHostAddress::Any, port))
        {
            g_log.report(QString(" Server now listening on port %1").arg(port));
            break;
        }
        else
        {
            g_log.report(QString(" Server failed to listen on port %1").arg(port));
        }
    } while(true);

    ping_timer.start();
}

void NetServer::acceptConnection()
{
    static int unknown_contacts = 0;

    NetSubclient *subclient = new NetSubclient(this);

    g_log.report("NetServer::acceptConnection()");

    subclient->server = this;
    subclient->socket = server.nextPendingConnection();
    subclient->contact = g_contacts.findContactByAddress(subclient->socket->peerAddress().toString());

    // Connect a message-receive handler
    connect(subclient->socket, SIGNAL(readyRead()), &read_mapper, SLOT(map()));
    read_mapper.setMapping(subclient->socket, (QObject *) subclient);
    connect(&read_mapper, SIGNAL(mapped(QObject *)), this, SLOT(startRead(QObject *)));
    connect(subclient->socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(gotError(QAbstractSocket::SocketError)));

    if (subclient->contact)
        g_log.report(QString(" Contact \"%1\" at %2").arg(subclient->contact->nickname).arg(subclient->contact->ip_address));
    else
    {
        g_log.report(QString(" Unknown contact at %1").arg(subclient->socket->peerAddress().toString()));

        if (g_contacts.getGroup(0))
        {
            Contact contact;

            g_log.report("Adding unknown contact");

            contact.ip_address = subclient->socket->peerAddress().toString();
            contact.nickname = QString("Unknown %1").arg(unknown_contacts);

            g_contacts.getGroup(0)->addContact(contact);
            subclient->contact = g_contacts.getGroup(0)->getContact(g_contacts.getGroup(0)->numContacts() - 1);
            unknown_contacts++;
        }
    }

    // Add 'em to the list of subclients
    subclients.append(subclient);

    emit connectionAccepted(subclient);
}

void NetServer::startRead(QObject *object)
{
    QByteArray data;

    g_log.report("NetServer::startRead()");

    NetSubclient *subclient = (NetSubclient *) object;
    if (!subclient)
        return;

    data = subclient->socket->readAll();

    g_log.report(QString(" Read %1 bytes").arg(data.length()));

    emit gotMessage(data, subclient);
}

void NetServer::sendData(QString targetIP, QByteArray data)
{
    NetSubclient *subclient = NULL;
    qint64 bytes_written = -1;

    g_log.report("NetServer::sendData()");

    subclient = findSubclient(targetIP);

    if (subclient && subclient->socket)
    {
        if (subclient->contact)
            g_log.report(QString(" Sending %1 bytes to \"%2\" at %3")
                         .arg(data.length())
                         .arg(subclient->contact->nickname)
                         .arg(targetIP));
        else
            g_log.report(QString(" Sending %1 bytes to unknown contact at %2")
                         .arg(data.length())
                         .arg(targetIP));

        // Send him the data
        bytes_written = subclient->socket->write(data);
        if (bytes_written == -1)
        {
            g_log.report(" Socket write failed");
        }
        else
        {
            g_log.report(QString(" Successfully sent %1 bytes").arg(bytes_written));
        }

        //subclient->socket->flush();
        //subclient->socket->waitForBytesWritten(1000);
    }
    else
    {
        g_log.report(QString(" Error: No target at \"%1\"").arg(targetIP));
    }
}

NetSubclient *NetServer::findSubclient(QString ip_address)
{
    NetSubclient *subclient = NULL;

    // Find the subclient by target IP
    for (int i=0; i<subclients.length(); i++)
    {
        subclient = subclients.at(i);

        if (!subclient || !subclient->contact)
            continue;
        if (subclient->contact->ip_address == ip_address)
        {
            return subclient;
        }
    }

    return NULL;
}

void NetServer::pingTimerEvent()
{
    int error = server.serverError();

    g_log.report("NetServer::pingTimerEvent()");

    if (error != -1)
    {
        g_log.report(QString(" Server error %1: %2")
                     .arg(error)
                     .arg(server.errorString()));
        return;
    }

/*    if (server.isListening())
    {
        Packet packet;

        packet.packet_type = Packet::PT_PING;
        packet.data = QByteArray(server.serverAddress().toString().toAscii());

        for (int i=0; i<subclients.length(); i++)
        {
            if (subclients[i]->socket)
            {
                packet.setCurrentTimestamp();
                subclients[i]->socket->write(packet.toByteArray());
            }
        }
    }*/
}

void NetServer::gotError(QAbstractSocket::SocketError error)
{
    int err = -1;
    QString nickname("Unknown");
    QString addr("???.???.???.???");

    g_log.report("NetServer::gotError()");

    for (int i=0; i<subclients.length(); i++)
    {
        if (subclients[i]->contact)
        {
            nickname = subclients[i]->contact->nickname;
            addr = subclients[i]->contact->ip_address;
        }

        err = subclients[i]->socket->error();

        if (err != -1)
        {
            g_log.report(QString(" Error %1 on subclient %2 at %3: %4")
                         .arg(err)
                         .arg(nickname)
                         .arg(addr)
                         .arg(subclients[i]->socket->errorString()));

            kickSubclient(i);
        }
    }
}

void NetServer::kickSubclient(int index)
{
    g_log.report("NetServer::kickSubclient()");

    if (index >= 0 && index < subclients.length())
    {
        g_log.report(QString(" Error: Invalid index: %1 (range 0 .. %2)").arg(index).arg(subclients.length()));

        return;
    }

    subclients[index]->socket->close();
    subclients.removeAt(index);
}
