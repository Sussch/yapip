#ifndef PORTCFGWIDGET_H
#define PORTCFGWIDGET_H

#include <QWidget>

namespace Ui {
    class PortCfgWidget;
}

class PortCfgWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PortCfgWidget(QWidget *parent = 0);
    ~PortCfgWidget();

    /**
     * Saves a list of ports
     */
    bool Save(QString path);

    /**
     * Loads a list of ports
     */
    bool Load(QString path);

signals:
    void accepted();
    void rejected();

public slots:
    void show();

private slots:
    void on_btnCancel_clicked();

    void on_btnOk_clicked();

    void on_btnAddPort_clicked();

    void on_listPorts_currentRowChanged(int currentRow);

    void on_spinPort_valueChanged(int arg1);

    void on_btnUp_clicked();

    void on_btnDown_clicked();

    void on_spinPort_editingFinished();

    void on_btnRemovePort_clicked();

private:
    Ui::PortCfgWidget *ui;
};

#endif // PORTCFGWIDGET_H
