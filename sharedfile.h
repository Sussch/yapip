#ifndef SHAREDFILE_H
#define SHAREDFILE_H

#include "contact.h"

#include <QList>

class SharedFile
{
public:
    SharedFile();

    SharedFile &operator=(const SharedFile &sf);

    Contact *source_contact;
    QString name;
    QString path;

    //! Paths of temporary files
    QList<QString> temp_paths;

    bool merge(QString target_path);

    void clear();
};

#endif // SHAREDFILE_H
