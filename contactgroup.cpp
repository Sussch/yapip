#include "contactgroup.h"
#include "netinterface.h"

ContactGroup::ContactGroup()
{
}

ContactGroup::ContactGroup(QString groupname) {
    setName(groupname);
}

ContactGroup::~ContactGroup()
{
    removeAllContacts();
}

bool ContactGroup::addContact(Contact contact)
{
    for (int i=0; i<contacts.length(); i++)
    {
        if (contacts.at(i).ip_address == contact.ip_address)
        {
            if (contacts.at(i).nickname == contact.nickname)
            {
                return false;
            }
            else
            {
                contacts[i].nickname = contact.nickname;
                return true;
            }
        }
    }

    contacts.append(contact);
    return true;
}

bool ContactGroup::removeContact(QString IPaddr)
{
    for (int i=0; i<contacts.length(); i++)
    {
        if (contacts.at(i).ip_address == IPaddr) {
            g_network.removeClient(IPaddr);
            contacts.removeAt(i);
            return true;
        }
    }
    return false;
}

bool ContactGroup::removeContact(int index)
{
    if (index >= 0 && index < contacts.length())
    {
        // Valgrind: Invalid read of size 4
        //           Address .... is 0 bytes inside a block of size 8 free'd
        g_network.removeClient(contacts.at(index).ip_address);
        contacts.removeAt(index);
        return true;
    }

    return false;
}

bool ContactGroup::hasContact(QString IPaddr) const
{
    for (int i=0; i<contacts.length(); i++)
    {
        if (contacts.at(i).ip_address == IPaddr)
            return true;
    }
    return false;
}

void ContactGroup::removeAllContacts()
{
    for (int i=0; i<contacts.length(); i++)
    {
        g_network.removeClient(contacts.at(i).ip_address);
    }

    contacts.clear();
}

int ContactGroup::numContacts() const
{
    return contacts.length();
}

Contact *ContactGroup::getContact(int index)
{
    if (index >= 0 && index < contacts.length())
        return &contacts[index];
    return NULL;
}

Contact *ContactGroup::getContact(QString IPaddr)
{
    for (int i=0; i<contacts.length(); i++)
    {
        if (contacts.at(i).ip_address == IPaddr)
            return &contacts[i];
    }
    return NULL;
}

QString ContactGroup::getName() const {
    return name;
}

void ContactGroup::setName(QString newname) {
    name = newname;
}
