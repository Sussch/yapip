#ifndef FILESHAREWIDGET_H
#define FILESHAREWIDGET_H

#include <QWidget>
#include <QSignalMapper>

namespace Ui {
class FileShareWidget;
}

class FileShareWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit FileShareWidget(QWidget *parent = 0);
    ~FileShareWidget();

signals:
    void accepted();
    void rejected();

    void acceptFile(QString name, QString sender);

public slots:
    void show();

    void addFile(QString name, QString sender);
    void removeFile(QString name);

private slots:
    void on_btnClose_clicked();

    void fileAccepted(QString filename);
    void fileRejected(QString filename);

private:
    Ui::FileShareWidget *ui;
    QSignalMapper accept_signal_mapper;
    QSignalMapper reject_signal_mapper;

    void init();
};

#endif // FILESHAREWIDGET_H
