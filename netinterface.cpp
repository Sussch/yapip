#include "netinterface.h"
#include "contacts.h"

#include "cryptinterface.h"
#include "log.h"

#include <iostream>

NetInterface g_network;

//! \bug Connection is single-sided when using crypto (with the exception of loopback). It says it's connectionAccepted, but no messages get through
//! \bug Fails on sending big files (10 KB) in Plaintext

NetInterface::NetInterface(QObject *parent) :
    QObject(parent)
{
    started = false;
}

NetInterface::~NetInterface()
{
    stop();
    server.clear();

    for (int i=0; i<clients.length(); i++)
        delete clients[i];

    clients.clear();
}

void NetInterface::start()
{
    // Make sure this function is only called once
    if (started)
        return;

    //! \bug It takes too long to cycle through the alternative ports
    // Should be free ports
    portconfig.portlist.append(3035);
/*    portconfig.portlist.append(3040);
    // Pokemon Netbattle port
    portconfig.portlist.append(30000);
    // HTTP port
    portconfig.portlist.append(80);
    // HTTPS port
    portconfig.portlist.append(443);
    portconfig.portlist.append(1863);*/

    server.portconfig = portconfig;

    // Encryption and decryption outputs
    connect(&g_crypt, SIGNAL(encOut(QByteArray,QString)), this, SLOT(sendMessage(QByteArray,QString)));
    connect(&g_crypt, SIGNAL(decOut(QByteArray,QString)), this, SLOT(routeMessage(QByteArray,QString)));

    connect(&server, SIGNAL(gotMessage(QByteArray,NetSubclient*)), this, SLOT(_messageToServer(QByteArray,NetSubclient*)));
    connect(&server, SIGNAL(connectionAccepted(NetSubclient*)), this, SLOT(connectionAcceptedSlot(NetSubclient*)));
    server.start();

    started = true;
}

void NetInterface::stop()
{
}

void NetInterface::postPacket(QString targetIP, Packet packet)
{
    // Timestamp the packet
    packet.setCurrentTimestamp();
    // Encrypt & send it
    g_crypt.decIn(packet.toByteArray(), targetIP);
}

void NetInterface::sendMessage(QByteArray data, QString targetIP)
{
    QByteArray temp;

    data = data.replace('^', "^^");
    data = data.replace('#', "^#");

    temp.append('#');
    temp.append(data);

    server.sendData(targetIP, temp);
}

void NetInterface::addClient(Contact *contact)
{
    // Make sure we have started the network interface before adding any clients
    if (!started)
        start();

    clients.append(new NetClient(this));

    if (clients.last())
    {
        connect(clients.last(), SIGNAL(gotMessage(QByteArray,NetClient*)), this, SLOT(_messageToClient(QByteArray,NetClient*)));
        connect(clients.last(), SIGNAL(gotConnected(NetClient*)), this, SLOT(gotConnectedSlot(NetClient*)));
        connect(clients.last(), SIGNAL(gotDisconnected(NetClient*)), this, SLOT(connectionLostSlot(NetClient*)));

        clients.last()->connectTo(contact);

        emit clientAdded(clients.last());
    }
}

void NetInterface::connectionAcceptedSlot(NetSubclient *subclient)
{
    g_log.report("NetInterface::connectionAcceptedSlot()");

    if (!subclient)
    {
        g_log.report(" Subclient pointer is NULL");
        return;
    }

    if (!subclient->contact)
    {
        g_log.report(" Subclient contact pointer is NULL");
        return;
    }

    g_log.report(QString(" Contact \"%1\" at %2")
                 .arg(subclient->contact->nickname)
                 .arg(subclient->contact->ip_address));

    // Forward the signal
    emit connectionAccepted(subclient);

    // There's no socket .. can't send anything yet
}

void NetInterface::_messageToServer(QByteArray data, NetSubclient *subclient)
{
    QByteArray temp;
    int last_index = 0;

    for (int i=0; i<data.length(); i++)
    {
        // Start of packet?
        if (data[i] == '#')
        {
            if (i > 0 && data[i - 1] == '^')
                continue;

            if (i - last_index <= 0)
            {
                last_index = i + 1;
                continue;
            }

            temp = data.mid(last_index, i - last_index);
            temp = temp.replace("^^", "^"); ///< \todo Does this work correctly?
            temp = temp.replace("^#", "#");

            g_log.report(QString(" Got message %1").arg(QString(temp.toHex())));

            //! \todo Support for multiple servers?
            g_crypt.encIn(temp, QString("S%1").arg(subclient->contact->ip_address));

            last_index = i + 1;
        }

        // End of message
        if (i == data.length() - 1)
        {
            if (i - last_index <= 0)
                break;

            temp = data.mid(last_index, i - last_index + 1);
            temp = temp.replace("^^", "^"); ///< \todo Does this work correctly?
            temp = temp.replace("^#", "#");

            g_log.report(QString(" Got message %1").arg(QString(temp.toHex())));

            //! \todo Support for multiple servers?
            g_crypt.encIn(temp, QString("S%1").arg(subclient->contact->ip_address));
        }
    }
}

void NetInterface::_messageToClient(QByteArray data, NetClient *client)
{
    QByteArray temp;
    int last_index = 0;

    for (int i=0; i<data.length(); i++)
    {
        // Start of packet?
        if (data[i] == '#')
        {
            if (i > 0 && data[i - 1] == '^')
                continue;

            if (i - last_index <= 0)
            {
                last_index = i + 1;
                continue;
            }

            temp = data.mid(last_index, i - last_index);
            temp = temp.replace("^^", "^"); ///< \todo Does this work correctly?
            temp = temp.replace("^#", "#");

            g_log.report(QString(" Got message %1").arg(QString(temp.toHex())));

            //! \todo Support for multiple servers?
            g_crypt.encIn(temp, QString("C%1").arg(client->target_contact->ip_address));

            last_index = i + 1;
        }

        // End of message
        if (i == data.length() - 1)
        {
            if (i - last_index <= 0)
                break;

            temp = data.mid(last_index, i - last_index + 1);
            temp = temp.replace("^^", "^"); ///< \todo Does this work correctly?
            temp = temp.replace("^#", "#");

            g_log.report(QString(" Got message %1").arg(QString(temp.toHex())));

            //! \todo Support for multiple servers?
            g_crypt.encIn(temp, QString("C%1").arg(client->target_contact->ip_address));
        }
    }
}

void NetInterface::routeMessage(QByteArray data, QString user_data)
{
    Packet packet;
    QString ip_addr = user_data.right(user_data.length() - 1);

    // Message from a server?
    if (user_data.at(0) == 'S')
    {
        NetSubclient *subclient = NULL;

        subclient = server.findSubclient(ip_addr);

        while(data.length() > packet.getOverhead())
        {
            packet.clear();
            data = packet.fromByteArray(data);

            emit serverGotPacket(packet, subclient);
        }

        //emit serverGotPacket(Packet(data), server.findSubclient(ip_addr));
    // Message from a client?
    }
    else if (user_data.at(0) == 'C')
    {
        NetClient *client = NULL;

        client = findClient(ip_addr);

        while(data.length() > packet.getOverhead())
        {
            packet.clear();
            data = packet.fromByteArray(data);

            emit clientGotPacket(packet, client);
        }

        //emit clientGotPacket(Packet(data), findClient(ip_addr));
    }
}

NetClient *NetInterface::findClient(QString ip_addr)
{
    for (int i=0; i<clients.length(); i++)
    {
        if (clients[i] && clients[i]->target_contact &&
            clients[i]->target_contact->ip_address == ip_addr)
            return clients[i];
    }
    return NULL;
}

void NetInterface::removeClient(QString ip_addr)
{
    int id = -1;

    for (int i=0; i<clients.length(); i++)
    {
        if (clients[i] && clients[i]->target_contact &&
            clients[i]->target_contact->ip_address == ip_addr)
        {
            delete clients[i];
            id = i;
            break;
        }
    }

    if (id > -1)
        clients.removeAt(id);
}

void NetInterface::connectionLostSlot(NetClient *client)
{
    // Forward the signal
    emit connectionLost(client);
}

void NetInterface::gotConnectedSlot(NetClient *client)
{
    // Forward the signal
    emit gotConnected(client);
}
