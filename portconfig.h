#ifndef PORTCONFIG_H
#define PORTCONFIG_H

#include <QObject>
#include <QList>

/**
 * @class PortConfig
 * Port allocation and iteration
 */
class PortConfig : public QObject
{
    Q_OBJECT
public:
    explicit PortConfig(QObject *parent = 0);

    PortConfig &operator=(const PortConfig &pcfg);

    /**
     * Iterates on the ports
     * @return New port
     */
    int nextPort();
    /**
     * Marks a port as used (hidden from nextPort)
     * @param[in] port Port to be marked as used
     */
    void markUsed(int port);

    /**
     * @class Port
     * Port descriptor
     */
    class Port
    {
    public:
        Port(int portnum);

        Port &operator=(const Port &p);

        //! Port number
        int port;
        //! Port already closed by another connection, a firewall or some other program?
        bool closed;
    };

    //! List of available ports
    QList<Port> portlist;

    /**
     * Initializes the set of ports
     */
    void initPorts();

    /**
     * Initializes the default set of ports
     */
    void initDefaultPorts();

signals:
    
public slots:

private:
    //! Current port iteration index
    int port_id;
};

#endif // PORTCONFIG_H
