#include "texteventfilter.h"
#include <QKeyEvent>
#include <iostream>

TextEventFilter::TextEventFilter(QObject *parent) :
    QObject(parent)
{
}

bool TextEventFilter::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

        if (keyEvent->key() == Qt::Key_Return)
        {
            emit sendText();
            return true;
        }
    }

    return QObject::eventFilter(obj, event);
}
