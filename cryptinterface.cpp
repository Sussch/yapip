#include "cryptinterface.h"

#include "cryptopp561/cryptlib.h"
#include "cryptopp561/gcm.h"
#include "cryptopp561/aes.h"
#include "cryptopp561/osrng.h"

#include "log.h"

#include <iostream>

using namespace CryptoPP;

CryptInterface g_crypt;

CryptInterface::CryptInterface(QObject *parent) :
    QObject(parent)
{
    // Default is not encrypted
    num_keys = 0;
    algorithm = CT_PlainText;
}

void CryptInterface::setNumKeys(int num)
{
    num_keys = num;
}

int CryptInterface::getNumKeys() const
{
    return num_keys;
}

void CryptInterface::setAlgorithm(Algorithm algo)
{
    algorithm = algo;
}

void CryptInterface::addKey(QByteArray key)
{
    //! \todo Check if the key has the correct length (if not, append something interesting / trim it)
    keys.append(key);
}

bool CryptInterface::removeKey(int index)
{
    if (index >= 0 && index < keys.length())
    {
        keys.removeAt(index);
        return true;
    }
    return false;
}

void CryptInterface::decIn(QByteArray data, QString user_data)
{
    g_log.report("CryptInterface::decIn()");

    if (algorithm == CT_PlainText)
    {
        g_log.report(" Bypassing as PlainText");
        emit encOut(data, user_data);
    }
    else if (algorithm == CT_AES)
    {
        std::string buf;

        if (keys.length() < 1)
        {
            g_log.report(" Error: No keys have been generated for AES GCM");
            return;
        }

        g_log.report(" Encrypting with AES GCM");

        try
        {
            GCM<AES>::Encryption enc_gcm_aes;
            //! valgrind: Use of uninitialised value of size 4 (key length?)
            enc_gcm_aes.SetKeyWithIV((const byte *) keys.at(0).data(), keys.at(0).length(), (const byte *) iv_data.data());

            AuthenticatedEncryptionFilter aef(enc_gcm_aes, new StringSink(buf));

            aef.Put((byte *) data.data(), data.length());
            //! \todo Make non-blocking?
            aef.MessageEnd();

            g_log.report(" Done");

            emit encOut(QByteArray(buf.c_str(), buf.length()), user_data);
        }
        catch(InvalidKeyLength ivkl)
        {
            g_log.report(" Exception: InvalidKeyLength");
        }
        catch(HashVerificationFilter::HashVerificationFailed hvf)
        {
            g_log.report(" Exception: HashVerificationFailed");
        }
    }
}

void CryptInterface::encIn(QByteArray data, QString user_data)
{
    g_log.report("CryptInterface::encIn()");

    if (algorithm == CT_PlainText)
    {
        g_log.report(" Bypassing as PlainText");
        emit decOut(data, user_data);
    }
    else if (algorithm == CT_AES)
    {
        std::string buf;

        if (keys.length() < 1)
        {
            g_log.report(" Error: No keys have been generated for AES GCM");
            return;
        }

        g_log.report(" Decrypting with AES GCM");

        try
        {
            GCM<AES>::Decryption dec_gcm_aes;
            dec_gcm_aes.SetKeyWithIV((const byte *)keys.at(0).data(), keys.at(0).length(), (const byte *)iv_data.data());

            AuthenticatedDecryptionFilter adf(dec_gcm_aes, new StringSink(buf));

            adf.Put((byte *)data.data(), data.length());
            //! \todo Make non-blocking?
            adf.MessageEnd();

            g_log.report(" Done");

            emit decOut(QByteArray(buf.c_str(), buf.length()), user_data);
        }
        catch(InvalidKeyLength ivkl)
        {
            g_log.report(" Exception: InvalidKeyLength");
        }
        catch(HashVerificationFilter::HashVerificationFailed hvf)
        {
            g_log.report(" Exception: HashVerificationFailed");
        }
    }
}

void CryptInterface::setIV(QByteArray iv)
{
    iv_data = iv;
}

void CryptInterface::genKeys()
{
    AutoSeededRandomPool rnd;
    QByteArray buf;

    g_log.report("CryptInterface::genKeys()");
    g_log.report(QString(" Generating %1 keys").arg(num_keys));

    buf.resize(AES::DEFAULT_KEYLENGTH);

    for (int i=0; i<num_keys; i++)
    {
        SecByteBlock key(AES::DEFAULT_KEYLENGTH);
        rnd.GenerateBlock((byte *)buf.data(), AES::DEFAULT_KEYLENGTH);

        keys.append(buf);
    }

    g_log.report(" Done");
}

QByteArray CryptInterface::getKey(int id) const
{
    return keys.at(id);
}

void CryptInterface::clearKeys()
{
    keys.clear();
}

QByteArray CryptInterface::formatKey(QByteArray key) const
{
    int i;
    int num_spaces = 0;
    int min = 16, max = 32;

    if (algorithm == CT_AES)
    {
        min = AES::MIN_KEYLENGTH;
        max = AES::MAX_KEYLENGTH;
    }

    // Key too short? Append whitespaces.
    if (key.length() < min)
    {
        num_spaces = min - key.length();
        for (i=0; i<num_spaces; i++)
            key.append(QString(" "));

        return key;
    }

    // Key longer than minimum but shorter than maximum?
    // Round it up to maximum by appending whitespaces.
    if (key.length() < max)
    {
        num_spaces = max - key.length();
        for (i=0; i<num_spaces; i++)
            key.append(QString(" "));

        return key;
    }

    // Key too long? Trim the last bytes.
    return key.left(max);
}
