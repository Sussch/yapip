#include "encryptsettingswidget.h"
#include "ui_encryptsettingswidget.h"
#include "cryptinterface.h"

#include "cryptopp561/aes.h"
#include "cryptopp561/osrng.h"

#include "log.h"

#include <QtXml/QDomDocument>

#include <iostream>

#include <QFileDialog>
#include <QTextStream>
#include <QTime>
#include <QMessageBox>

using namespace CryptoPP;

EncryptSettingsWidget::EncryptSettingsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EncryptSettingsWidget)
{
    ui->setupUi(this);

    connect(&key_filter, SIGNAL(sendText()), this, SLOT(edit_key()));

    ui->editKey->installEventFilter(&key_filter);
    ui->tabWidget->setCurrentIndex(0);

    populateAlgorithms();
    populateIVSources();
}

EncryptSettingsWidget::~EncryptSettingsWidget()
{
    delete ui;
}

void EncryptSettingsWidget::populateAlgorithms()
{
    ui->comboEncType->addItem(QString("PlainText"));
    ui->comboEncType->addItem(QString("AES"));

    ui->comboEncType->setCurrentIndex(0);
}

void EncryptSettingsWidget::populateIVSources()
{
    ui->comboIVSource->addItem(QString("Pseudo-random"));
    ui->comboIVSource->addItem(QString("Type gibberish")); //! \todo Implement
    ui->comboIVSource->addItem(QString("Microphone")); //! \todo Implement

    ui->comboIVSource->setCurrentIndex(0);
}

void EncryptSettingsWidget::show()
{
    ui->listKeys->clear();

    for (int i=0; i<g_crypt.getNumKeys(); i++)
    {
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(QString(g_crypt.getKey(i)));
        item->setData(Qt::UserRole, QVariant(g_crypt.getKey(i)));

        ui->listKeys->addItem(item);
    }

    QWidget::show();
}

void EncryptSettingsWidget::on_btnOk_clicked()
{
    g_crypt.clearKeys();

    for (int i=0; i<ui->listKeys->count(); i++)
    {
        g_crypt.addKey(ui->listKeys->item(i)->data(Qt::UserRole).toByteArray());
    }

    saveSettings("encryption.xml");

    emit accepted();
    hide();
}

void EncryptSettingsWidget::on_btnCancel_clicked()
{
    emit rejected();
    hide();
}

void EncryptSettingsWidget::on_comboEncType_currentIndexChanged(int index)
{
    if (ui->comboEncType->itemText(index) == "PlainText")
    {
        g_crypt.setAlgorithm(CryptInterface::CT_PlainText);
        // No keys needed
        ui->spinNumKeys->setMinimum(0);
    }
    else if (ui->comboEncType->itemText(index) == "AES")
    {
        g_crypt.setAlgorithm(CryptInterface::CT_AES);

        // There has to be at least 1 key
        ui->spinNumKeys->setMinimum(1);
        // Show key length range
        ui->labelKeyLenMin->setText(QString("%1").arg(AES::MIN_KEYLENGTH));
        ui->labelKeyLenMax->setText(QString("%1").arg(AES::MAX_KEYLENGTH));
    }
}

void EncryptSettingsWidget::on_spinNumKeys_valueChanged(int arg1)
{
    g_crypt.setNumKeys(arg1);
}

void EncryptSettingsWidget::on_btnGenRndKeys_clicked()
{
    g_crypt.genKeys();

    ui->listKeys->clear();

    for (int i=0; i<ui->spinNumKeys->value(); i++)
    {
        //! \todo Memory leak?
        QListWidgetItem *item = new QListWidgetItem(ui->listKeys);
        QByteArray data = g_crypt.getKey(i);
        //data.toHex();
        item->setText(QString(data));
        item->setData(Qt::UserRole, QVariant(data));

        ui->listKeys->addItem(item);
    }

    ui->listKeys->setCurrentRow(0);
}

void EncryptSettingsWidget::on_listKeys_currentRowChanged(int currentRow)
{
    if (currentRow >= 0 && currentRow < ui->listKeys->count())
        ui->editKey->document()->setPlainText(ui->listKeys->item(currentRow)->text());
}

void EncryptSettingsWidget::on_btnSaveKeys_clicked()
{
    QString path;
    QByteArray line;

    // Just export the keys

    path = QFileDialog::getSaveFileName(this, QString("List of keys"), QString(), QString("*.keys"));

    if (path.length() > 4)
    {
        QFile file(path);

        //! \todo The network settings should be an XML file with keys embedded

        if (!file.open(QIODevice::WriteOnly))
        {
            QString msg(QString("Failed to write to file \"%1\"").arg(path));
            g_log.report("EncryptSettingsWidget::on_btnSaveKeys_clicked()");
            g_log.report(QString(" Error: %1").arg(msg));
            QMessageBox::critical(this, "File error", msg);
            file.close();
            return;
        }

        for (int i=0; i<ui->listKeys->count(); i++)
        {
            line = ui->listKeys->item(i)->data(Qt::UserRole).toByteArray().toHex();
            file.write(line);
            file.write(QByteArray("\r\n"));
        }

        file.close();
    }
}

void EncryptSettingsWidget::on_btnLoadKeys_clicked()
{
    QString path;
    QByteArray line, data;

    // Just import the keys

    path = QFileDialog::getOpenFileName(this, QString("List of keys"), QString(), QString("*.keys"));

    if (path.length() > 4)
    {
        QFile file(path);

        //! \todo The network settings should be an XML file with keys embedded

        if (!file.open(QIODevice::ReadOnly))
        {
            QString msg(QString("Failed to read from file \"%1\"").arg(path));
            g_log.report("EncryptSettingsWidget::on_btnLoadKeys_clicked()");
            g_log.report(QString(" Error: %1").arg(msg));
            QMessageBox::critical(this, "File error", msg);
            file.close();
            return;
        }

        // Clear the old keys
        ui->listKeys->clear();

        QTextStream in(&file);
        while(!in.atEnd())
        {
            line = in.readLine().toAscii();
            data = QByteArray::fromHex(line);

            //! \todo Memory leak?
            QListWidgetItem *item = new QListWidgetItem();
            item->setText(QString(data));
            item->setData(Qt::UserRole, QVariant(data));

            ui->listKeys->addItem(item);
        }

        // Activate the first key
        ui->listKeys->setCurrentRow(0);
        ui->spinNumKeys->setValue(ui->listKeys->count());

        file.close();
    }
}

void EncryptSettingsWidget::on_btnAddKey_clicked()
{
    QString key = ui->editKey->toPlainText();
    bool ok = false;
    int row = 0;
    int numspaces = 0;

    int min_key_len = ui->labelKeyLenMin->text().toInt(&ok, 10);
    if (!ok)
    {
        g_log.report("EncryptSettingsWidget::on_btnAddKey_clicked()");
        g_log.report(" Error: ui->labelKeyLenMin->text() is not a number");
        return;
    }

    int max_key_len = ui->labelKeyLenMax->text().toInt(&ok, 10);
    if (!ok)
    {
        g_log.report("EncryptSettingsWidget::on_btnAddKey_clicked()");
        g_log.report(" Error: ui->labelKeyLenMax->text() is not a number");
        return;
    }

    // Make sure the key meets the crypt requirements
    key = QString(g_crypt.formatKey(key.toAscii()));

    //! \todo Memory leak? Test by adding and removing keys.
    QListWidgetItem *item = new QListWidgetItem();
    item->setText(key);
    item->setData(Qt::UserRole, QVariant(key));

    ui->listKeys->addItem(item);
    ui->spinNumKeys->setValue(ui->listKeys->count());
}

void EncryptSettingsWidget::on_btnRemoveKey_clicked()
{
    if (ui->listKeys->currentItem())
    {
        ui->listKeys->takeItem(ui->listKeys->currentRow());
        ui->spinNumKeys->setValue(ui->listKeys->count());
    }
}

void EncryptSettingsWidget::on_btnKeyUp_clicked()
{
    QString text;
    QByteArray data;
    int id = ui->listKeys->currentRow();

    if (id - 1 < 0)
        return;

    text = ui->listKeys->currentItem()->text();
    data = ui->listKeys->currentItem()->data(Qt::UserRole).toByteArray();
    ui->listKeys->item(id)->setText(ui->listKeys->item(id - 1)->text());
    ui->listKeys->item(id)->setData(Qt::UserRole, ui->listKeys->item(id - 1)->data(Qt::UserRole));
    ui->listKeys->item(id - 1)->setText(text);
    ui->listKeys->item(id - 1)->setData(Qt::UserRole, data);
    ui->listKeys->setCurrentRow(id - 1);
}

void EncryptSettingsWidget::on_btnKeyDown_clicked()
{
    QString text;
    QByteArray data;
    int id = ui->listKeys->currentRow();

    if (id + 1 > ui->listKeys->count())
        return;

    text = ui->listKeys->currentItem()->text();
    data = ui->listKeys->currentItem()->data(Qt::UserRole).toByteArray();
    ui->listKeys->item(id)->setText(ui->listKeys->item(id + 1)->text());
    ui->listKeys->item(id)->setData(Qt::UserRole, ui->listKeys->item(id + 1)->data(Qt::UserRole));
    ui->listKeys->item(id + 1)->setText(text);
    ui->listKeys->item(id + 1)->setData(Qt::UserRole, data);
    ui->listKeys->setCurrentRow(id + 1);
}

bool EncryptSettingsWidget::saveSettings(QString path)
{
    int i;
    bool ok = false;

    g_log.report("EncryptSettingsWidget::saveSettings()");
    g_log.report(QString(" Path: \"%1\"").arg(path));

    QDomDocument document("cryptsettings");
    QDomElement root = document.createElement("cryptsettings");

    document.appendChild(root);

    QDomElement elem_algorithm = document.createElement("algorithm");
    elem_algorithm.setAttribute("name", ui->comboEncType->currentText());
    elem_algorithm.setAttribute("id", ui->comboEncType->currentIndex());
    root.appendChild(elem_algorithm);

    QDomElement elem_IV = document.createElement("IV");
    elem_IV.setAttribute("sourceName", ui->comboIVSource->currentText());
    elem_IV.setAttribute("sourceID", ui->comboIVSource->currentIndex());
    elem_IV.setAttribute("value", ui->editIV->text());
    root.appendChild(elem_IV);

    QDomElement elem_keys = document.createElement("keys");
    elem_keys.setAttribute("numKeys", ui->spinNumKeys->value());
    elem_keys.setAttribute("keyHoppingEnabled", ui->checkKeyHopping->checkState());

    elem_keys.setAttribute("minKeyLength", ui->labelKeyLenMin->text().toInt(&ok));
    if (!ok)
    {
        g_log.report(" Error: ui->labelKeyLenMin->text() is not a number");
    }

    elem_keys.setAttribute("maxKeyLength", ui->labelKeyLenMax->text().toInt(&ok));
    if (!ok)
    {
        g_log.report(" Error: ui->labelKeyLenMax->text() is not a number");
    }

    for (i=0; i<ui->listKeys->count(); i++)
    {
        QDomElement elem_key = document.createElement("key");
        elem_key.setAttribute("value", QString(ui->listKeys->item(i)->data(Qt::UserRole).toByteArray().toHex()));
        elem_keys.appendChild(elem_key);
    }

    root.appendChild(elem_keys);

    // Open the file for writing
    QFile file(path);
    if (!file.open(QIODevice::WriteOnly))
    {
        QString msg(QString("Failed to write to file %1").arg(path));
        g_log.report(QString(" Error: %1").arg(msg));
        QMessageBox::critical(this, QString("File error"), msg);
        return false;
    }

    // Write to file
    QTextStream stream(&file);
    stream << document.toString();

    file.close();

    return true;
}

bool EncryptSettingsWidget::loadSettings(QString path, bool quiet)
{
    int i;
    QString error_msg;
    QByteArray data;
    QString text;
    bool ok = false;
    int error_line, error_column;

    g_log.report("EncryptSettingsWidget::loadSettings()");
    g_log.report(QString(" Path: \"%1\"").arg(path));

    QDomDocument document(QString("cryptsettings"));
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly))
    {
        if (quiet)
        {
            g_log.report(QString(" Settings file \"%1\" doesn't exist yet")
                         .arg(path));
        }
        else
        {
            QString msg(QString("Failed to read from file %1").arg(path));
            g_log.report(QString(" Error: %1").arg(msg));
            QMessageBox::critical(this, QString("File error"), msg);
        }

        return false;
    }

    if (!document.setContent(&file, &error_msg, &error_line, &error_column))
    {
        g_log.report(QString(" XML Syntax error: %1, line %2, column %3:")
                     .arg(path)
                     .arg(error_line)
                     .arg(error_column));
        g_log.report(QString("  %1")
                     .arg(error_msg));

        if (!quiet)
        {
            QMessageBox::critical(this, QString("XML error"),
                                  QString("Syntax error in the settings file \"%1\".\r\nSee the log for details.")
                                  .arg(path));
        }

        file.close();
        return false;
    }

    // The DOM tree is now in memory
    file.close();

    // Is this a correct contactlist file?
    QDomElement root = document.documentElement();
    if (root.tagName() != "cryptsettings")
    {
        QString msg(QString("The file \"%1\" does not contain encryption settings.")
                    .arg(path));
        g_log.report(QString(" Error: %1").arg(msg));

        if (!quiet)
        {
            QMessageBox::critical(this, QString("XML error"), msg);
        }

        return false;
    }

    // For each element
    QDomElement element = root.firstChildElement();
    while (!element.isNull())
    {
        // Algorithm?
        if (element.tagName() == "algorithm")
        {
            ui->comboEncType->setCurrentIndex(element.attribute("id", "").toInt(&ok));
        }
        // IV?
        else if (element.tagName() == "IV")
        {
            ui->comboIVSource->setCurrentIndex(element.attribute("sourceID", "").toInt(&ok));

            text = element.attribute("value", "");
            data = QByteArray::fromHex(text.toAscii());
            ui->editIV->setText(text);
            // Set the IV in the crypt interface
            g_crypt.setIV(data);
        }
        // Keys?
        else if (element.tagName() == "keys")
        {
            ui->spinNumKeys->setValue(element.attribute("numKeys", "").toInt(&ok));

            if (!ok)
            {
                g_log.report(" Error: Element attribute numKeys is not a number");
            }

            ui->labelKeyLenMin->setText(QString("%1")
                                        .arg(element.attribute("minKeyLength", "").toInt(&ok)));
            if (!ok)
            {
                g_log.report(" Error: Element attribute minKeyLength is not a number");
            }

            ui->labelKeyLenMax->setText(QString("%1")
                                        .arg(element.attribute("maxKeyLength", "").toInt(&ok)));
            if (!ok)
            {
                g_log.report(" Error: Element attribute maxKeyLength is not a number");
            }

            ui->checkKeyHopping->setCheckState((Qt::CheckState) element.attribute("keyHoppingEnabled", "").toInt(&ok));

            if (!ok)
            {
                g_log.report(" Error: Element attribute keyHoppingEnabled is not a number");
            }

            // Clear the old keys
            ui->listKeys->clear();
            // Clear the keys in the crypt interface as well
            g_crypt.clearKeys();

            QDomElement subelement = element.firstChildElement();
            while(!subelement.isNull())
            {
                // Key?
                if (subelement.tagName() == "key")
                {
                    text = subelement.attribute("value", "");
                    data = QByteArray::fromHex(text.toAscii());

                    //! \todo Memory leak?
                    QListWidgetItem *item = new QListWidgetItem();
                    item->setText(QString(data));
                    item->setData(Qt::UserRole, QVariant(data));

                    ui->listKeys->addItem(item);

                    // Load the key into the crypt interface
                    g_crypt.addKey(data);
                }

                subelement = subelement.nextSiblingElement();
            }

            // Activate the first key
            ui->listKeys->setCurrentRow(0);
        }

        element = element.nextSiblingElement();
    }

    return true;
}

void EncryptSettingsWidget::on_btnSaveSettings_clicked()
{
    QString path;

    path = QFileDialog::getSaveFileName(this, QString("Crypt settings"), QString(), QString("*.xml"));

    if (path.length() > 3)
    {
        saveSettings(path);
    }
}

void EncryptSettingsWidget::on_btnGenerateIV_clicked()
{
    QByteArray buf, hex;
    buf.resize(AES::BLOCKSIZE);

    g_log.report("EncryptSettingsWidget::on_btnGenerateIV_clicked()");
    g_log.report(QString(" Generating from \"%1\"")
                 .arg(ui->comboIVSource->currentText()));

    //! \todo Implement the rest
    if (ui->comboIVSource->currentText() == "Pseudo-random")
    {
        AutoSeededRandomPool rnd;

        rnd.GenerateBlock((byte *)buf.data(), AES::BLOCKSIZE);
        hex = buf.toHex();
        ui->editIV->setText(QString(hex));

        g_crypt.setIV(buf);
    }
}

void EncryptSettingsWidget::on_btnLoadSettings_clicked()
{
    QString path;

    path = QFileDialog::getOpenFileName(this, QString("Crypt settings"), QString(), QString("*.xml"));

    if (path.length() > 3)
    {
        loadSettings(path);
    }
}

void EncryptSettingsWidget::edit_key()
{
    QString text;
    QByteArray data;

    if (ui->listKeys->currentItem())
    {
        text = ui->editKey->toPlainText();
        data = text.toAscii();

        ui->listKeys->currentItem()->setText(text);
        ui->listKeys->currentItem()->setData(Qt::UserRole, QVariant(data));
    }
}

void EncryptSettingsWidget::on_editKey_textChanged()
{
    ui->labelKeyLen->setText(QString("%1").arg(ui->editKey->toPlainText().length()));
}
