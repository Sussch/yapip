#ifndef ENCRYPTSETTINGSWIDGET_H
#define ENCRYPTSETTINGSWIDGET_H

#include <QWidget>
#include <QTextDocument>
#include <QPlainTextDocumentLayout>

#include "texteventfilter.h"

namespace Ui {
class EncryptSettingsWidget;
}

class EncryptSettingsWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit EncryptSettingsWidget(QWidget *parent = 0);
    ~EncryptSettingsWidget();

    /**
     * Saves settings with IV and keys
     * @param[in] path Settings file path
     * @return True on success, false on failure.
     */
    bool saveSettings(QString path);
    /**
     * Loads settings with IV and keys
     * @param[in] path Settings file path
     * @return True on success, false on failure.
     */
    bool loadSettings(QString path, bool quiet = false);

signals:
    void accepted();
    void rejected();

public slots:
    void show();

private slots:
    void on_btnOk_clicked();

    void on_btnCancel_clicked();

    /**
     * Reconfigure the settings panes for algorithm-specific settings
     */
    void on_comboEncType_currentIndexChanged(int index);
    /**
     * A change in the number of keys to be generated
     */
    void on_spinNumKeys_valueChanged(int arg1);

    void on_btnGenRndKeys_clicked();

    void on_listKeys_currentRowChanged(int currentRow);

    void on_btnSaveKeys_clicked();

    void on_btnLoadKeys_clicked();

    void on_btnAddKey_clicked();

    void on_btnRemoveKey_clicked();

    void on_btnKeyUp_clicked();

    void on_btnKeyDown_clicked();

    void on_btnSaveSettings_clicked();

    void on_btnGenerateIV_clicked();

    void on_btnLoadSettings_clicked();

    void edit_key();

    void on_editKey_textChanged();

private:
    Ui::EncryptSettingsWidget *ui;

    TextEventFilter key_filter;

    /**
     * Fills the algorithms combobox with choices
     */
    void populateAlgorithms();
    /**
     * Fills the IV combobox with choices
     */
    void populateIVSources();
};

#endif // ENCRYPTSETTINGSWIDGET_H
