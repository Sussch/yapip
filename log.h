#ifndef LOG_H
#define LOG_H

#include <QObject>
#include <QList>

class Log : public QObject
{
    Q_OBJECT
public:
    explicit Log(QObject *parent = 0);
    
    void setPath(QString path);

signals:
    
public slots:
    void report(QString message);
    void close();
private:
    //! \todo Have it thread-safe
    //QList<QString> log_queue;
    QString logfile_path;
    bool open;
};

//! A global instance of the log
extern Log g_log;

#endif // LOG_H
