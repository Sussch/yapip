#ifndef NETCLIENT_H
#define NETCLIENT_H

#include <QObject>
#include <QtNetwork/QTcpSocket>
#include <QTimer>

#include "portconfig.h"
#include "contact.h"

class NetClient : public QObject
{
    Q_OBJECT
public:
    explicit NetClient(QObject *parent = 0);
    ~NetClient();
    
    NetClient &operator=(NetClient *client);

    bool isConnected();

    QString getTargetName() const;
    QString getTargetIP() const;

    //! Target contact
    Contact *target_contact;

    //! \todo Use QSslSocket
    //! Connection socket
    QTcpSocket socket;

    //! Port configuration
    PortConfig portconfig;

signals:
    /**
     * Signal of message reception
     * @param[in] message Message that was received
     * @param[in] subclient Pointer to the subclient that sent it
     */
    void gotMessage(QByteArray message, NetClient *client);

    /**
     * Signal of connection established
     * @param[in] client Pointer to the client that was connected
     */
    void gotConnected(NetClient *client);

    /**
     * Signal of connection lost
     * @param[in] client Pointer to the client that was disconnected
     */
    void gotDisconnected(NetClient *client);

public slots:
    void connectTo(Contact *target);
    void connectToTarget();

private slots:
    void connected();
    void disconnected();
    void error(QAbstractSocket::SocketError socketError);

    void reconnect();

    void startRead();

private:
    //! Are we connected?
    bool connection_ok;

    //! Reconnecting timer
    QTimer reconnect_timer;
};

#endif // NETCLIENT_H
