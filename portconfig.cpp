#include "portconfig.h"

#include <iostream>

PortConfig::PortConfig(QObject *parent) :
    QObject(parent)
{
    port_id = -1;
}

PortConfig::Port::Port(int portnum)
{
    port = portnum;
    closed = false;
}

PortConfig::Port &PortConfig::Port::operator=(const Port &p)
{
    port = p.port;
    closed = p.closed;

    return *this;
}

PortConfig &PortConfig::operator=(const PortConfig &pcfg)
{
    portlist.clear();
    for (int i=0; i<pcfg.portlist.length(); i++)
    {
        portlist.append(pcfg.portlist.at(i));
    }
    //portlist = pcfg.portlist;

    return *this;
}

int PortConfig::nextPort()
{
    if (portlist.empty())
        return -1;

    //! \todo Figure out a better way to get the correct port

    int id = (port_id + 1) % portlist.length();

    // Try to find a port that is not closed yet
    for (int i=0; i<portlist.length(); i++)
    {
        if (!portlist[id].closed)
        {
            port_id = id;
            return portlist[id].port;
        }

        id = (id + 1) % portlist.length();
    }

    port_id = id;
    return -1;
}

void PortConfig::markUsed(int port)
{
    for (int i=0; i<portlist.length(); i++)
    {
        if (portlist[i].port == port)
        {
            portlist[i].closed = true;
        }
    }
}
