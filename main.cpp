#include "mainwindow.h"
#include "contacts.h"
#include "log.h"

#include <QtGui/QApplication>

int main(int argc, char *argv[])
{
    int retval = 0;
    QApplication app(argc, argv);

    g_log.report("");
    g_log.report("** YapIP started");

    // Used for application settings
    QCoreApplication::setOrganizationName("Raring Visions");
    QCoreApplication::setApplicationName("YapIP");

    MainWindow mainWindow;
    mainWindow.setOrientation(MainWindow::ScreenOrientationAuto);
    mainWindow.showExpanded();

    retval = app.exec();

    g_contacts.save("contacts.xml");

    g_log.report("** YapIP closed");
    g_log.report("");
    g_log.close();

    return retval;
}
