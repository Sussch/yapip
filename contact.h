#ifndef CONTACT_H
#define CONTACT_H

#include <QString>

class Contact
{
public:
    Contact();
    ~Contact();

    //! Contact name
    QString nickname;
    //! Contact IP address
    QString ip_address;
};

#endif // CONTACT_H
