#include "log.h"

#include <QFile>
#include <QTime>
#include <QDate>
#include <QThread>

Log g_log;

Log::Log(QObject *parent) :
    QObject(parent)
{
    logfile_path = QString("yapip.log");
    open = true;
}

void Log::setPath(QString path)
{
    logfile_path = path;
}

void Log::report(QString message)
{
    // Ignore reports when the log has already been closed
    if (!open)
        return;

    QFile file(logfile_path);

    if (!file.open(QIODevice::Append))
    {
        //! \todo Do something!
        return;
    }

    // Add thread handle, date & time and newline
    message = QString("[%1] %2 %3: ")
            .arg((int) QThread::currentThreadId())
            .arg(QDate::currentDate().toString())
            .arg(QTime::currentTime().toString())
            + message + QString("\r\n");

    file.write(message.toAscii());

    file.close();
}

void Log::close()
{
    open = false;
}
