#include "sharedfile.h"
#include "log.h"

#include <QtAlgorithms>
#include <QFile>

SharedFile::SharedFile()
{
}

SharedFile &SharedFile::operator=(const SharedFile &sf)
{
    name = sf.name;
    path = sf.path;
    source_contact = sf.source_contact;
    return *this;
}

bool SharedFile::merge(QString target_path)
{
    int i, j;
    QByteArray data;

    g_log.report("SharedFile::merge()");

    QFile fout(target_path);
    if (!fout.open(QIODevice::WriteOnly))
    {
        g_log.report(QString(" Error: Failed to write to file \"%1\"")
                     .arg(target_path));
        return false;
    }

    // Sort the temporary files by timestamp
    qSort(temp_paths.begin(), temp_paths.end());

    // Merge the files
    for (i=0; i<temp_paths.length(); i++)
    {
        QFile file(temp_paths[i]);

        g_log.report(QString(" Appending file \"%1\"")
                     .arg(temp_paths[i]));

        if (!file.open(QIODevice::ReadOnly))
        {
            g_log.report(QString(" Error: Failed to read from file \"%1\"")
                         .arg(temp_paths[i]));
            continue;
        }

        data = file.readAll();
        fout.write(data);

        file.close();
    }

    fout.close();
    return true;
}

void SharedFile::clear()
{
    name = "";
    path = "";
    source_contact = NULL;
    temp_paths.clear();
}
