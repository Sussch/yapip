#ifndef ADDGROUPWIDGET_H
#define ADDGROUPWIDGET_H

#include <QWidget>

namespace Ui {
class AddGroupWidget;
}

class AddGroupWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit AddGroupWidget(QWidget *parent = 0);
    ~AddGroupWidget();

signals:
    void accepted();
    void rejected();

public slots:
    void show();

private slots:
    void on_btnOk_clicked();

    void on_btnCancel_clicked();

private:
    Ui::AddGroupWidget *ui;
};

#endif // ADDGROUPWIDGET_H
