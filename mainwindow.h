#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QtGui/QMainWindow>
#include <QStandardItemModel>
#include <QModelIndex>
#include <QItemSelection>
#include <QTextDocument>
#include <QList>

#include "texteventfilter.h"
#include "contact.h"
#include "netsubclient.h"
#include "netclient.h"
#include "packet.h"

#include "sharedfile.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    enum ScreenOrientation {
        ScreenOrientationLockPortrait,
        ScreenOrientationLockLandscape,
        ScreenOrientationAuto
    };

    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();

    // Note that this will only have an effect on Symbian and Fremantle.
    void setOrientation(ScreenOrientation orientation);

    void showExpanded();

    /**
     * Saves conversation log to file
     * @return True on success, false otherwise
     */
    bool saveLog(QString path);

public slots:
    void updateContacts();
    /**
     * Send the message that has been written into the textbox
     */
    void sendMessage();
    /**
     * Start sharing a file
     * @param[in] path Path of the file to be shared
     */
    void startFileshare(QString path);
    /**
     * Log a message by a certain subclient
     * @param[in] packet Received packet
     * @param[in] subclient Pointer to the subclient that forwarded it
     */
    void serverGotPacket(Packet packet, NetSubclient *subclient);
    /**
     * Log a message by a certain subclient
     * @param[in] packet Received packet
     * @param[in] client Pointer to the client
     */
    void clientGotPacket(Packet packet, NetClient *client);

private slots:
    /**
     * Handler for clicks on a group or contact
     */
    void contactsClicked(QModelIndex mid);
    /**
     * Handler for context menu requests
     * \note Only works for desktop version
     * @param[in] p The point where the context menu is requested
     */
    void contextMenu(const QPoint &p);
    /**
     * Pops up the "Add contact" dialog
     */
    void addContact();
    /**
     * Pops up the "Add group" dialog
     */
    void addGroup();
    /**
     * Pops up the "Edit contact" dialog
     */
    void editContact();

    /**
     * Issued when the addContact widget is accepted
     */
    void addContactAccepted();
    /**
     * Issued when the addContact widget is rejected
     */
    void addContactRejected();
    /**
     * Issued when the addGroup widget is accepted
     */
    void addGroupAccepted();
    /**
     * Issued when the addGroup widget is rejected
     */
    void addGroupRejected();

    /**
     * Clicked Ok in the about widget?
     */
    void aboutAccepted();

    /**
     * Clicked Ok in the portconfig widget?
     */
    void portsAccepted();
    /**
     * Clicked Cancel in the portconfig widget?
     */
    void portsRejected();

    /**
     * Clicked Ok in the encryption settings widget?
     */
    void encSettingsAccepted();
    /**
     * Clicked Cancel in the encryption settings widget?
     */
    void encSettingsRejected();

    void acceptFile(QString filename, QString sender);

    void clientAdded(NetClient *client);

    /**
     * Handler for new connections
     * @param[in] subclient Pointer to the subclient that connected
     */
    void connectionAccepted(NetSubclient *subclient);
    /**
     * Signal of connection established
     * @param[in] client Pointer to the client that was connected
     */
    void gotConnected(NetClient *client);
    /**
     * Handler for lost connections
     * @param[in] client Pointer to the client that got disconnected
     */
    void connectionLost(NetClient *client);

    /**
     * Handler for changes in the selection of contacts / groups
     */
    void contactsSelected(QItemSelection selected, QItemSelection deselected);

    /**
     * Switches to the chat window
     */
    void startChat();

    /**
     * Menu actions
     */
    void on_actionAdd_contact_triggered();
    void on_actionAdd_group_triggered();
    void on_actionRemove_contacts_triggered();

    void on_actionStart_a_conversation_triggered();

    void on_actionSave_contacts_triggered();

    void on_actionLoad_contacts_triggered();

    void on_actionSave_conversation_triggered();

    void on_actionAbout_triggered();

    void on_actionPort_config_triggered();

    void on_actionEncryption_config_triggered();

    void on_chatTabs_currentChanged(int index);

    void on_btnAddGroup_clicked();

    void on_btnAddContact_clicked();

    void on_btnSendFile_clicked();

    void sendFile(QString targetIP, QString filename);

    void on_actionEdit_contact_triggered();

private:
    Ui::MainWindow *ui;

    enum StackItem
    {
        ST_Contacts = 0,
        ST_About = 1,
        ST_Ports = 2,
        ST_EncryptionSettings = 3
    };

    QPoint spawnpoint;

    //! Chat log document
    QTextDocument chat_log;
    //! Filter for sending text (emits sendMessage signal on Return)
    TextEventFilter text_filter;

    //! List of selected contacts
    QList<Contact *> active_contacts;

    //! StackWidget index (for temporary storage)
    int stackwidget_id;

    //! List of files currently downloading
    QList<SharedFile> files_downloading;
    //! List of files currently uploading
    QList<SharedFile> files_uploading;

    QList<QStandardItem *> selected_groups;
    QList<QStandardItem *> selected_contacts;

    SharedFile *getDownloadingFileByAddr(QString sourceIP);
    SharedFile *getDownloadingFileByName(QString fname);
    SharedFile *getUploadingFileByAddr(QString sourceIP);
    SharedFile *getUploadingFileByName(QString fname);

    /**
     * Rebuilds the tree of contacts
     */
    void refreshContactsTree();
    /**
     * Initializes the stackup widget
     */
    void initStackup();
    /**
     * Initializes the tree of contacts (adds a default group)
     */
    void initContactsTree();
    /**
     * Initializes network (sets up a server)
     */
    void initNetwork();
    /**
     * Initializes the chat pane
     */
    void initChat();

    /**
     * Appends the text to log
     */
    void appendToLog(QString text);

    /**
     * Formats a message for displaying in the log window
     * @param[in] whosays   Nickname of the sender
     * @param[in] color     The color of the sender
     * @param[in] message   What did he say?
     */
    QString formatMessage(QString whosays, QString color, QString message);

    /**
     * Extracts target contact names from a message like "@Sussch, Ted, Paul: Let's discuss the interfaces."
     * @param[out] message Parsed message (target contact names excluded)
     * @return List of target names. For the current example, ["Sussch", "Ted", "Paul"].
     */
    QStringList parseTargetNames(QString &message);

    /**
     * @class ClassifyItem
     * Classifies items in the tree of contacts
     */
    class ClassifyItem
    {
    public:
        /**
         * Constructor
         * @param[in] sid Modelview item identifier string
         */
        ClassifyItem(QString sid);

        /**
         * Checks if it's a group item
         */
        bool isGroup();
        /**
         * Checks if it's a contact item
         */
        bool isContact();
        /**
         * Gets the group / contact index from the item identifier string
         */
        int getIndex();
        /**
         * Gets the group / contact name from the item identifier string
         */
        QString getName();

    private:
        //! Tokens of the item identifier string
        QStringList item_sid;
        //! Index that is extracted from the identifier
        int index;
    };
};

#endif // MAINWINDOW_H
