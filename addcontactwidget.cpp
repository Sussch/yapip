#include "addcontactwidget.h"
#include "ui_addcontactwidget.h"

#include "contacts.h"
#include "netinterface.h"

AddContactWidget::AddContactWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddContactWidget)
{
    ui->setupUi(this);

    connect(ui->editIP, SIGNAL(returnPressed()), this, SLOT(on_btnAdd_clicked()));

    hide();
}

AddContactWidget::~AddContactWidget()
{
    delete ui;
}

void AddContactWidget::show()
{
    ui->editNick->setFocus();

    QWidget::show();
}

void AddContactWidget::on_btnAdd_clicked()
{
    ContactGroup *group = NULL;
    Contact contact;

    group = g_contacts.getGroup(g_contacts.active_group);

    if (group) {
        contact.nickname = ui->editNick->text();
        contact.ip_address = ui->editIP->text();

        group->addContact(contact);

        // Add a client for this contact
        g_network.addClient(group->getContact(group->numContacts() - 1));
    }

    emit accepted();
    hide();
}

void AddContactWidget::on_btnCancel_clicked()
{
    emit rejected();
    hide();
}

void AddContactWidget::setNickname(QString name)
{
    ui->editNick->setText(name);
}

void AddContactWidget::setIPAddress(QString IPaddr)
{
    ui->editIP->setText(IPaddr);
}
