#ifndef NETSUBCLIENT_H
#define NETSUBCLIENT_H

#include <QObject>
#include <QtNetwork/QTcpSocket>

#include "contact.h"

/**
 * @class NetSubclient
 * A remote client that is connected to one of our servers
 */
class NetSubclient : public QObject
{
    Q_OBJECT
public:
    explicit NetSubclient(QObject *parent = 0);
    
    //! Contact associated with this subclient (based on IP comparison)
    Contact *contact;
    //! Connection socket
    QTcpSocket *socket;

    //! Pointer to the parent server
    class NetServer *server;

signals:
    
public slots:
};

#endif // NETSUBCLIENT_H
