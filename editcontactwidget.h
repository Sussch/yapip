#ifndef EDITCONTACTWIDGET_H
#define EDITCONTACTWIDGET_H

#include <QWidget>

#include "contact.h"

namespace Ui {
class EditContactWidget;
}

class EditContactWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit EditContactWidget(QWidget *parent = 0);
    ~EditContactWidget();

    void setContact(Contact *p_contact);

signals:
    void accepted();
    void rejected();

public slots:
    void show();

private slots:
    void on_btnApply_clicked();

    void on_btnCancel_clicked();
    
private:
    Ui::EditContactWidget *ui;
    Contact *contact;
};

#endif // EDITCONTACTWIDGET_H
