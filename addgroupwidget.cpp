#include "addgroupwidget.h"
#include "ui_addgroupwidget.h"

#include "contacts.h"

AddGroupWidget::AddGroupWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddGroupWidget)
{
    ui->setupUi(this);

    connect(ui->editGroupName, SIGNAL(returnPressed()), this, SLOT(on_btnOk_clicked()));

    hide();
}

AddGroupWidget::~AddGroupWidget()
{
    delete ui;
}

void AddGroupWidget::show()
{
    ui->editGroupName->setFocus();

    QWidget::show();
}

void AddGroupWidget::on_btnOk_clicked()
{
    ContactGroup group;

    group.setName(ui->editGroupName->text());

    g_contacts.addGroup(group);

    emit accepted();
    hide();
}

void AddGroupWidget::on_btnCancel_clicked()
{
    emit rejected();
    hide();
}
