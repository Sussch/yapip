#include "portcfgwidget.h"
#include "ui_portcfgwidget.h"
#include "netinterface.h"

#include <iostream>

PortCfgWidget::PortCfgWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PortCfgWidget)
{
    ui->setupUi(this);
    hide();
}

PortCfgWidget::~PortCfgWidget()
{
    delete ui;
}

void PortCfgWidget::show()
{
    ui->listPorts->clear();

    for (int i=0; i<g_network.portconfig.portlist.length(); i++)
    {
        ui->listPorts->addItem(QString("%1").arg(g_network.portconfig.portlist[i].port));
    }

    QWidget::show();
}

void PortCfgWidget::on_btnCancel_clicked()
{
    emit rejected();
    hide();
}

void PortCfgWidget::on_btnOk_clicked()
{
    g_network.portconfig.portlist.clear();

    // Make sure not to create any duplicates
    for (int i=0; i<ui->listPorts->count(); i++)
    {
        if (ui->listPorts->item(i))
        {
            g_network.portconfig.portlist.append(ui->listPorts->item(i)->text().toInt());
        }
    }

    // Reset the server (force it to start listening on a new port)
    g_network.server.restart();

    emit accepted();
    hide();
}

void PortCfgWidget::on_btnAddPort_clicked()
{
    QString portname = QString("%1").arg(ui->spinPort->value());

    // Make sure not to create any duplicates
    for (int i=0; i<ui->listPorts->count(); i++)
    {
        if (ui->listPorts->item(i)->text() == portname)
            return;
    }

    ui->listPorts->addItem(portname);
}

void PortCfgWidget::on_listPorts_currentRowChanged(int currentRow)
{
    if (currentRow >= 0 && currentRow < ui->listPorts->count())
        ui->spinPort->setValue(ui->listPorts->item(currentRow)->text().toInt());
}

void PortCfgWidget::on_spinPort_valueChanged(int arg1)
{
}

bool PortCfgWidget::Save(QString path)
{
    //! \todo Implement
    return true;
}

bool PortCfgWidget::Load(QString path)
{
    //! \todo Implement
    return true;
}

void PortCfgWidget::on_btnUp_clicked()
{
    QString temp;
    int id;

    if (ui->listPorts->currentRow() - 1 < 0)
        return;

    id = ui->listPorts->currentRow();
    temp = ui->listPorts->currentItem()->text();
    ui->listPorts->item(id)->setText(ui->listPorts->item(id - 1)->text());
    ui->listPorts->item(id - 1)->setText(temp);
    ui->listPorts->setCurrentRow(id - 1);
}

void PortCfgWidget::on_btnDown_clicked()
{
    QString temp;
    int id;

    if (ui->listPorts->currentRow() + 1 >= ui->listPorts->count())
        return;

    id = ui->listPorts->currentRow();
    temp = ui->listPorts->currentItem()->text();
    ui->listPorts->item(id)->setText(ui->listPorts->item(id + 1)->text());
    ui->listPorts->item(id + 1)->setText(temp);
    ui->listPorts->setCurrentRow(id + 1);
}

void PortCfgWidget::on_spinPort_editingFinished()
{
    if (ui->listPorts->currentItem())
        ui->listPorts->currentItem()->setText(QString("%1").arg(ui->spinPort->value()));
}

void PortCfgWidget::on_btnRemovePort_clicked()
{
    if (ui->listPorts->currentItem())
        ui->listPorts->takeItem(ui->listPorts->currentRow());
}
