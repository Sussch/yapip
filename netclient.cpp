#include "netclient.h"

#include <QtNetwork/QHostAddress>

#include "portconfig.h"
#include "netinterface.h"
#include "log.h"

#include <iostream>

NetClient::NetClient(QObject *parent) :
    QObject(parent)
{
    connection_ok = false;

    target_contact = NULL;

    portconfig = g_network.portconfig;

    connect(&socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(&socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(&socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error(QAbstractSocket::SocketError)));
    connect(&socket, SIGNAL(readyRead()), this, SLOT(startRead()));

    reconnect_timer.setInterval(3000);
    connect(&reconnect_timer, SIGNAL(timeout()), this, SLOT(reconnect()));
}

NetClient::~NetClient()
{
    reconnect_timer.stop();
    socket.close();
}

void NetClient::connectTo(Contact *target)
{
    //! \bug Crashes on reloading contacts

    g_log.report("NetClient::connectTo()");

    if (!target)
    {
        g_log.report(" Error: Invalid target contact pointer");
        return;
    }

    int port = portconfig.nextPort();

    g_log.report(QString(" Contact \"%1\" at \"%2\" on port %3")
                 .arg(target->nickname)
                 .arg(target->ip_address)
                 .arg(port));

    if (port == -1)
        return;

    target_contact = target;
    socket.connectToHost(QHostAddress(target->ip_address), port);
}

void NetClient::connectToTarget()
{
    connectTo(target_contact);
}

bool NetClient::isConnected()
{
    return connection_ok;
}

void NetClient::connected()
{
    connection_ok = true;

    g_log.report("NetClient::connected()");
    g_log.report(QString(" Target \"%1\" at %2 on port %3")
                 .arg(getTargetName())
                 .arg(socket.peerAddress().toString())
                 .arg(socket.peerPort()));

    emit gotConnected(this);
}

void NetClient::disconnected()
{
    connection_ok = false;

    g_log.report("NetClient::disconnected()");
    g_log.report(QString(" Target \"%1\" was at %2 on port %3")
                 .arg(getTargetName())
                 .arg(getTargetIP())
                 .arg(socket.peerPort()));

    emit gotDisconnected(this);
    reconnect_timer.start();
}

void NetClient::error(QAbstractSocket::SocketError socketError)
{   
    g_log.report("NetClient::error()");
    g_log.report(QString(" Contact \"%1\" at %2 on port %3")
                 .arg(getTargetName())
                 .arg(getTargetIP())
                 .arg(socket.peerPort()));
    g_log.report(QString(" Error %1: %2")
                 .arg(socketError)
                 .arg(socket.errorString()));

    reconnect_timer.start();
}

void NetClient::reconnect()
{
    if (socket.state() == QAbstractSocket::UnconnectedState)
    {
        g_log.report("NetClient::reconnect()");
        connectToTarget();
    }
}

void NetClient::startRead()
{
    QByteArray data;

    g_log.report("NetClient::startRead()");
    g_log.report(QString(" Contact \"%1\" at %2 on port %3")
                 .arg(getTargetName())
                 .arg(getTargetIP())
                 .arg(socket.peerPort()));

    data = socket.readAll();

    g_log.report(QString(" Read %1 bytes").arg(data.length()));
    g_log.report(QString(" Data: %1").arg(QString(data.toHex())));

    emit gotMessage(data, this);
}

QString NetClient::getTargetName() const
{
    if (target_contact)
        return target_contact->nickname;
    return "Unknown";
}

QString NetClient::getTargetIP() const
{
    if (target_contact)
        return target_contact->ip_address;
    return "???.???.???.???";
}
